/* Raster Blaster Pinball Remake
 *
 * Copyright (c) 2015 waynejohnson.net
 *
 * This software is licensed under the 
 * Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
 * http://creativecommons.org/licenses/by-nc-sa/4.0/
 * 
 * While the license does not allow commercial use, I permit the 
 * use (but not sale) of the software commercially.
 * This license will change over time to fit more appropriately.
 * 
 */

/**
 * @date started January 2015, finished ---
 * @author sausage@zeta.org.au
 *
 */
//#define _CONSOLE_ true

#include "orx.h"
#include "_pinball.h"
#include "element.h"
#include "bumperelement.h"
#include "slingshotelement.h"
#include "targettopelement.h"
#include "targetrightelement.h"
#include "targetrightelement.h"
#include "rolloverlightelement.h"
#include "directionrolloverelement.h"
#include "plunger.h"
#include "pinballbase.h"
#include "rasterblaster.h"
#include <sstream>
#include <vector>
#if defined(WIN32)
//#include <winver.h>
#endif

#define HIPPOCKET_PINBALL_ICON  1

/** Run callback for standalone
 */
orxSTATUS orxFASTCALL Run()
{
  //orxLOG("run() +");
  orxSTATUS eResult = orxSTATUS_SUCCESS;
  if(orxInput_IsActive("Quit")){
    eResult = orxSTATUS_FAILURE;
  }

  //orxLOG("run() -");
  RasterBlaster::instance()->ProcessAndroidAccelerometer();

  return eResult;
}

void orxFASTCALL Exit()
{ 
	orxLOG("=================== EXIT ================");
}

/** Inits the tutorial
 */
orxSTATUS orxFASTCALL Init()
{
	
	//PinballBase::create();
	//PinballBase::instance()->Init();
	
/*	if(orxConfig_PushSection("KeysForInput")) {
		orxConfig_SetString("JOY_X_1", "AccelX");
		orxConfig_SetString("JOY_Y_1", "AccelY");
		orxConfig_SetString("JOY_Z_1", "AccelZ");
		orxConfig_SetString("KEY_LCTRL", "LeftFlipper");
		orxConfig_PopSection();
	}
	
	orxConfig_ReloadHistory();*/
	
	//Or uncomment below to use the derived version
	RasterBlaster::create();
	RasterBlaster::instance()->Init();		
	
	return orxSTATUS_SUCCESS; 
	
}



int main(int argc, char **argv)
{
	#ifndef __orxDEBUG__ 
		orxConfig_SetEncryptionKey("0000-1111");
	#endif // __orxDEBUG__
	
	orx_Execute(argc, argv, Init, Run, Exit);

	return EXIT_SUCCESS;
}

#ifdef __orxMSVC__

// Here's an example for a console-less program under windows with visual studio
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
  // Inits and executes orx
  orx_WinExecute(Init, Run, Exit);

  // Done!
  return EXIT_SUCCESS;
}

#endif // __orxMSVC__

/* Registers plugin entry */
//orxPLUGIN_DECLARE_ENTRY_POINT(Init);


