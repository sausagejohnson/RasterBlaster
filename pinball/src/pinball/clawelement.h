#ifndef CLAWELEMENT_H
#define CLAWELEMENT_H

class clawelement : public element {

private:
	orxBOOL clawIsActive;
	orxBOOL ballIsCaptured;
	orxOBJECT *capturedBall;
	
public:
	clawelement(	int points, orxSTRING activatorNameFromConfig, orxSTRING activatorAnimationNameFromConfig, 
					orxSTRING activatorHitAnimationNameFromConfig, orxSTRING lightNameFromConfig);
	~clawelement();
	
	void CompressClaw();
	void ActivateClaw();
	void DeactivateClaw();
	void SetCapturedBall(orxOBJECT *ball);
	orxOBJECT* GetCapturedBall();
	void DropCapturedBall();
	void DestroyCapturedBall(); //DropCapturedBall is prefered. DestroyCapturedBall is for gameover.
	
	orxBOOL ClawIsActive();
	orxBOOL BallIsCaptured();
	
	orxSTRING GetClawType();
};

#endif // CLAWELEMENT_H
