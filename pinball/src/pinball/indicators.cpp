#include "orx.h"
#include "indicators.h"

indicators::indicators()
{
	object1x = orxObject_CreateFromConfig("1xObject");
	object2x = orxObject_CreateFromConfig("2xObject");
	object3x = orxObject_CreateFromConfig("3xObject");
	object4x = orxObject_CreateFromConfig("4xObject");
	object5x = orxObject_CreateFromConfig("5xObject");
	
	object1 = orxObject_CreateFromConfig("1Object");
	object5 = orxObject_CreateFromConfig("5Object");
	object10 = orxObject_CreateFromConfig("10Object");
	object50 = orxObject_CreateFromConfig("50Object");
	object100 = orxObject_CreateFromConfig("100Object");
	
	objectR = orxObject_CreateFromConfig("RObject");
	objectB = orxObject_CreateFromConfig("BObject");
	
	objectExtraBall = orxObject_CreateFromConfig("ExtraBallObject");
	objectTilt = orxObject_CreateFromConfig("TiltObject");
	
	orxObject_Enable(objectTilt, orxFALSE);
	
	bonus = 0;
	bonusSnapshotAtGameOver = 0;
	
	multiplierValue = 1;
	SetMultiplier(multiplierValue);

	extraBallPending = orxFALSE;

	TurnAllBonusLightsOff();
	TurnRBLightsOff();
}

void indicators::Flash1(){
	orxObject_Enable(object1, orxTRUE);
	orxObject_AddFX(object1, "IndicatorFlashFX");	
}

void indicators::Flash5(){
	orxObject_Enable(object5, orxTRUE);
	orxObject_AddFX(object5, "IndicatorFlashFX");	
}

void indicators::Flash10(){
	orxObject_Enable(object10, orxTRUE);
	orxObject_AddFX(object10, "IndicatorFlashFX");	
}

void indicators::Flash50(){
	orxObject_Enable(object50, orxTRUE);
	orxObject_AddFX(object50, "IndicatorFlashFX");	
}

void indicators::Flash100(){
	orxObject_Enable(object100, orxTRUE);
	orxObject_AddFX(object100, "IndicatorFlashFX");	
}

void indicators::FlashR(){
	orxObject_Enable(objectR, orxTRUE);
	orxObject_AddFX(objectR, "IndicatorFlashFX");	
}

void indicators::FlashB(){
	orxObject_Enable(objectB, orxTRUE);
	orxObject_AddFX(objectB, "IndicatorFlashFX");	
}

void indicators::FlashExtraBall(){
	orxObject_Enable(objectExtraBall, orxTRUE);
	orxObject_AddFX(objectExtraBall, "IndicatorFlashThenOffFX");	
}


void indicators::FlashTilt(){
	orxObject_Enable(objectTilt, orxTRUE);
	orxObject_AddFX(objectTilt, "IndicatorFlashThenOffFX");	
}


void indicators::SetMultiplier(int number){
	
	if (number < 1 || number > 5){
		if (multiplierValue == 5 && number > 5){
			orxObject_Enable(objectExtraBall, orxTRUE);
			extraBallPending = orxTRUE;
		}
		return;
	}
	
	TurnAllMultiplierLightsOff();
	

	
	if (number != multiplierValue){
		multiplierValue = number;
	}
	
	switch (number) {
		case 1:
			orxObject_Enable(object1x, orxTRUE);
			break;
		case 2:
			orxObject_Enable(object2x, orxTRUE);
			break;
		case 3:
			orxObject_Enable(object3x, orxTRUE);
			break;
		case 4:
			orxObject_Enable(object4x, orxTRUE);
			break;
		case 5:
			orxObject_Enable(object5x, orxTRUE);
			break;
	}
}

/* Valid values are 5000, 10000 or 15000, etc */
void indicators::AddToBonus(int bonusIncrease){
	//int previousValue = value;
	
	TurnAllBonusLightsOff();
	
	
	bonus += bonusIncrease;
	orxFLOAT small = orxFLOAT(bonus)/1000;
	
	orxLOG("BONUS: %d", bonus);
	
	
	//int difference = number - previousValue;
	
	orxFLOAT val = 0;
	
	if (small > 165){
		Flash100();
		Flash50();
		Flash10();
		Flash5();
		return;
	}
	
	if (small == 100){
		Flash100();
		return;
	}
	
	if (small == 50){
		Flash50();
		return;
	}
	
	if (small == 10){
		Flash10();
		return;
	}
	
	if (small == 5){
		Flash5();
		return;
	}
	
	int remaining = small; //used to check the smaller chunk
	
	if (small >= 100){
		val = small/100;
		int flattened = orxU32(val);
		if (flattened == 1) {
			Flash100();
		}
		remaining = small - 100;
	}
	
	if (remaining >= 50 && remaining < 100){
		val = remaining/50;
		int flattened = orxU32(val);
		if (flattened == 1) {
			Flash50();
		}
		remaining = remaining - 50;
	}
	
	
	if (remaining == 5 || remaining >= 15 ){
		Flash5();
	}
	
	if (remaining >= 10){
		Flash10();
	}
	

	
}


void indicators::SnapshotBonus(){
	if (bonusSnapshotAtGameOver != 0){
		return; //can only be set once.
	}
	
	bonusSnapshotAtGameOver = bonus;
}


int indicators::GetMultiplier(){
	return multiplierValue;
}


void indicators::TurnAllMultiplierLightsOff(){
	orxObject_Enable(object1x, orxFALSE);
	orxObject_Enable(object2x, orxFALSE);
	orxObject_Enable(object3x, orxFALSE);
	orxObject_Enable(object4x, orxFALSE);
	orxObject_Enable(object5x, orxFALSE);
	orxObject_Enable(objectExtraBall, orxFALSE);
}

void indicators::TurnAllBonusLightsOff(){
	orxObject_Enable(object1, orxFALSE);
	orxObject_Enable(object5, orxFALSE);
	orxObject_Enable(object10, orxFALSE);
	orxObject_Enable(object50, orxFALSE);
	orxObject_Enable(object100, orxFALSE);
}


void indicators::ResetAll(){
	bonus = 0;
	bonusSnapshotAtGameOver = 0;
	TurnAllLightsOff();
	SetMultiplier(1);
}


void indicators::ResetAllExceptMultiplier(){
	bonus = 0;
	bonusSnapshotAtGameOver = 0;
	TurnAllBonusLightsOff();
	TurnRBLightsOff();

	extraBallPending = orxFALSE;
}


void indicators::ClearBonusAndLights(){
	bonus = 0;
	bonusSnapshotAtGameOver = 0;
	TurnAllBonusLightsOff();
}


void indicators::TurnRBLightsOff(){
	orxObject_Enable(objectR, orxFALSE);
	orxObject_Enable(objectB, orxFALSE);
}

void indicators::TurnAllLightsOff(){
	TurnAllBonusLightsOff();
	TurnAllMultiplierLightsOff();
	TurnRBLightsOff();
	
	extraBallPending = orxFALSE;
}

void indicators::SetExtraBallPending(orxBOOL onOff){
	extraBallPending = onOff;
}

orxBOOL indicators::IsROn(){
	return orxObject_IsEnabled(objectR);
}

orxBOOL indicators::IsBOn(){
	return orxObject_IsEnabled(objectB);
}

orxBOOL indicators::IsExtraBallPending(){
	return extraBallPending;
}

void indicators::ParentTo(orxOBJECT *parent){
	orxObject_SetParent(object1x, parent);
	orxObject_SetParent(object2x, parent);
	orxObject_SetParent(object3x, parent);
	orxObject_SetParent(object4x, parent);
	orxObject_SetParent(object5x, parent);
	orxObject_SetParent(object1, parent);
	orxObject_SetParent(object5, parent);
	orxObject_SetParent(object10, parent);
	orxObject_SetParent(object50, parent);
	orxObject_SetParent(object100, parent);
	orxObject_SetParent(objectR, parent);
	orxObject_SetParent(objectB, parent);
	orxObject_SetParent(objectExtraBall, parent);
	orxObject_SetParent(objectTilt, parent);
}


indicators::~indicators()
{
}

