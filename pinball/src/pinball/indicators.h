#ifndef INDICATORS_H
#define INDICATORS_H

class indicators
{
public:
	indicators();
	~indicators();
	
	void Flash1();
	void Flash5();
	void Flash10();
	void Flash50();
	void Flash100();
	
	void FlashR();
	void FlashB();
	
	void FlashExtraBall();
	void FlashTilt();

	void TurnAllMultiplierLightsOff();
	void TurnAllBonusLightsOff();
	void ClearBonusAndLights(); //clears the bonus value before calling TurnAllBonusLightsOff()
	void TurnRBLightsOff();
	void TurnAllLightsOff();
	void ResetAll(); //clears all lights and bonus completely.
	void ResetAllExceptMultiplier();
	
	void SetExtraBallPending(orxBOOL onOff);

	void SetMultiplier(int number);
	int GetMultiplier();
	
	void AddToBonus(int bonusIncrease);
	
	void SnapshotBonus(); //copy bonus value. Used at gameover time.
	
	void ParentTo(orxOBJECT *parent); //set all objects in the indicator to be children of passed in object.
	
	int bonus; //stored as the actual bonus value: 5000, 15000, 200000 etc
	int bonusSnapshotAtGameOver; //copy of the last bonus value at game over. Used to calc diff from bonus to animate the multiplier down.
	
	orxBOOL IsROn();
	orxBOOL IsBOn();
	orxBOOL IsExtraBallPending();

private:

	orxOBJECT *object1x;
	orxOBJECT *object2x;
	orxOBJECT *object3x;
	orxOBJECT *object4x;
	orxOBJECT *object5x;
	
	orxOBJECT *object1;
	orxOBJECT *object5;
	orxOBJECT *object10;
	orxOBJECT *object50;
	orxOBJECT *object100;
	
	orxOBJECT *objectR;
	orxOBJECT *objectB;
	
	orxOBJECT *objectExtraBall;
	orxOBJECT *objectTilt;
	
	int multiplierValue;
	orxBOOL extraBallPending;

};

#endif // INDICATORS_H
