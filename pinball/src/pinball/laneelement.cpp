#include "orx.h"
#include "element.h"
#include "laneelement.h"

laneelement::laneelement() : element(0, (orxCHAR*)"LaneObject", orxNULL)
{
	activatorAnimationName = (orxCHAR*)"LaneOffAnim";
	activatorHitAnimationName = (orxCHAR*)"LaneOnAnim";
	lightOnAnimationName = orxNULL;
}

laneelement::~laneelement()
{
}

