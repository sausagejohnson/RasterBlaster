#ifndef RASTERBLASTER_H
#define RASTERBLASTER_H

#include "pinballbase.h"
#include "spinnerelement.h"
#include "clawelement.h"
#include "shieldelement.h"
#include "indicators.h"

class RasterBlaster : public PinballBase 
{

	
public:
	static void create();
	virtual void Init();
	
	virtual void PrintScore();	
	virtual void PrintScore(int overriddenScore); //use to not use "score" by default.
	
	virtual void TurnOffAllMultiplierLights();
	virtual void TurnOffAllTableLights();
	virtual void StartNewGame();
	virtual void DestroyBall(orxOBJECT *ballObject);
	virtual void DestroyBallAndCreateNewBall(orxOBJECT *ballObject); //need override to ensure the correct indicators are set on new ball

	virtual void AddToScore(int points);
	
	orxBOOL targetGroupTopLeftFilled;
	orxBOOL targetGroupTopRightFilled;
	
	clawelement *leftClaw;
	clawelement *rightClaw;
	clawelement *topClaw;
	
	int clawMode; //1 is two claws, 2 is three claws. 
	orxU64 displayedScore;

	
	spinnerelement *spinner;

	shieldelement *leftShield;
	shieldelement *rightShield;
	
	elementgroup *targetGroupTopLeft;
	elementgroup *targetGroupTopRight;

	indicators *indicatorLights; 
	
	int freeShieldSeconds;
	const int FREE_SHIELD_SECONDS = 20;

	static const int SLINGSHOT_VARIANCE = 400;

	void MakeElementAChildOfTheTable(element *e);


protected:
	RasterBlaster();
	virtual ~RasterBlaster();
	
	virtual orxSTATUS orxFASTCALL TextureLoadingEventHandler(const orxEVENT *_pstEvent);
	virtual orxSTATUS orxFASTCALL PhysicsEventHandler(const orxEVENT *_pstEvent);
	virtual orxSTATUS orxFASTCALL AnimationEventHandler(const orxEVENT *_pstEvent);
	virtual orxSTATUS orxFASTCALL InputEventHandler(const orxEVENT *_pstEvent);
	virtual orxSTATUS orxFASTCALL CustomEventHandler(const orxEVENT *_pstEvent);
	virtual void orxFASTCALL SecondsUpdate(const orxCLOCK_INFO *_pstClockInfo, void *_pstContext);
	virtual void orxFASTCALL ScoreUpdater(const orxCLOCK_INFO *_pstClockInfo, void *_pstContext);

	virtual void ProcessEventsBasedOnScore();
	virtual void ProcessAndroidAccelerometer();

	virtual void ProcessBallAndLeftSlingShotCollision(orxOBJECT *ballObject, orxOBJECT *leftSlingShotObject);
	virtual void ProcessBallAndRightSlingShotCollision(orxOBJECT *ballObject, orxOBJECT *rightSlingShotObject);
	virtual void ProcessBallAndShieldCollision(orxOBJECT *ballObject, orxOBJECT *shieldObject);
	virtual void ProcessBallAndClawCollision(orxOBJECT *ballObject, orxOBJECT *clawObject);
	virtual void ProcessBallAndTargetTopCollision(orxOBJECT *ballObject, orxOBJECT *targetLeftObject);
	virtual void ProcessBallAndTargetRightCollision(orxOBJECT *ballObject, orxOBJECT *targetRightObject);

	virtual void Tilt();
	
private:
	void LaunchMultiBall(); //ie have all claws drop their balls
	orxBOOL AllClawsFilled(); //ie have all claws got a ball?

	orxBOOL protectLeftShield; //are they on because of achievement, or just the free 10 seconds?
	orxBOOL protectRightShield; 

};

#endif // RASTERBLASTER_H
