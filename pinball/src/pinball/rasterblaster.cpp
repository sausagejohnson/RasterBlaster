#include "orx.h"
#include "elementgroup.h"
#include "element.h"
#include "bumperelement.h"
#include "slingshotelement.h"
#include "targettopelement.h"
#include "targetrightelement.h"
#include "targetrightelement.h"
#include "rolloverlightelement.h"
#include "directionrolloverelement.h"
#include "laneelement.h"
#include "shieldelement.h"
#include "plunger.h"
#include "rasterblaster.h"
#include <sstream>
#include <vector>

RasterBlaster::RasterBlaster() : PinballBase()
{
	scorePadding = "000000";	
	
	bumperStrength = 800;
	slingShotVector = {1200, -250, 0};
	plungerStrengthMultiplier = 30;

	freeShieldSeconds = FREE_SHIELD_SECONDS;
	protectLeftShield = orxFALSE;
	protectRightShield = orxFALSE;
}

RasterBlaster::~RasterBlaster()
{
	
}

void RasterBlaster::create()
{
	if (_instance)
		int here = 1;
	else
		_instance = new RasterBlaster();

}

void RasterBlaster::Init() {

	//orxConfig_Load("../rasterblaster.ini"); //load overrides 	
	
	
	bumperelement *be1 = new bumperelement(100);
	be1->SetActivatorPosition(518, 270);	
	//be1->SetActivatorPosition(358, 270);	
	
	//262+ from all fixed to relative.
	//or +160
	
	
	
	bumperelement *be2 = new bumperelement(100);
	//be2->SetActivatorPosition(548, 305);	
	//be2->SetActivatorPosition(810, 305);	
	be2->SetActivatorPosition(708, 305);	
	
	bumperelement *be3 = new bumperelement(100);
	//be3->SetActivatorPosition(358, 440);	
	be3->SetActivatorPosition(518, 440);	
	
	bumperelement *be4 = new bumperelement(100);
	//be4->SetActivatorPosition(548, 473);	
	be4->SetActivatorPosition(708, 473);	
	
	slingshotelement *sle = new slingshotelement(500, (orxCHAR *)"SlingShotLeftObject", orxNULL);//, (orxCHAR *)"SlingShotLeftMountObject");
	sle->SetActivatorFX((orxCHAR *)"FXSlingShotLeft");
	slingshotelement *sre = new slingshotelement(500, (orxCHAR *)"SlingShotRightObject", orxNULL);//, (orxCHAR *)"SlingShotRightMountObject");
	sre->SetActivatorFX((orxCHAR *)"FXSlingShotRight");
	
	//orxObject_CreateFromConfig("BumperObject");
	
	
	leftClaw = new clawelement(100, (orxCHAR *)"LeftClawObject", (orxCHAR *)"LeftClawDecompressedIdle", 
											(orxCHAR *)"LeftClawCompressAnim", (orxCHAR*)"LeftClawArrowObject");
	leftClaw->SetLightFX((orxCHAR *)"ArrowFX");
	
	rightClaw = new clawelement(100, (orxCHAR *)"RightClawObject", (orxCHAR *)"RightClawDecompressedIdle", 
											(orxCHAR *)"RightClawCompressAnim", (orxCHAR*)"RightClawArrowObject");
	rightClaw->SetLightFX((orxCHAR *)"ArrowFX");
	
	topClaw = new clawelement(100, (orxCHAR *)"TopClawObject", (orxCHAR *)"TopClawDecompressedIdle", 
											(orxCHAR *)"TopClawCompressAnim", (orxCHAR*)"TopClawArrowObject");
	topClaw->SetLightFX((orxCHAR *)"ArrowFX");
	
	//orxObject_CreateFromConfig("TableTestLayoutObject");
	
	int tgX = 382;
	int tgY = 416;

	
	tgX = 914;
	tgY = 585;
	targetGroupRight = new elementgroup(10000);
	targetGroupRight->SetEventName((orxCHAR*)"TARGET_RIGHT_GROUP_FILLED");

	targetrightelement *te5 = new targetrightelement(400);
	te5->SetActivatorPosition(tgX+56, tgY+6);
	targetrightelement *te6 = new targetrightelement(400);
	te6->SetActivatorPosition(tgX+56, tgY+40);
	targetrightelement *te7 = new targetrightelement(400);
	te7->SetActivatorPosition(tgX+56, tgY+74);
	targetGroupRight->Add(te5);
	targetGroupRight->Add(te6);
	targetGroupRight->Add(te7);
	
	tgX = 438;
	tgY = 73;	
	
	rolloverlightgroup = new elementgroup(10000);
	rolloverlightgroup->SetEventName((orxCHAR*)"ROLLOVER_GROUP_FILLED");
	
	rolloverlightelement *ro1 = new rolloverlightelement(0);
	ro1->SetActivatorPosition(tgX, tgY);
	rolloverlightelement *ro2 = new rolloverlightelement(0);
	ro2->SetActivatorPosition(tgX + 95, tgY+13);
	rolloverlightelement *ro3 = new rolloverlightelement(0);
	ro3->SetActivatorPosition(tgX + 190, tgY+26);
	rolloverlightelement *ro4 = new rolloverlightelement(0);
	ro4->SetActivatorPosition(tgX + 280, tgY+39);
	rolloverlightgroup->Add(ro1);
	rolloverlightgroup->Add(ro2);
	rolloverlightgroup->Add(ro3);
	rolloverlightgroup->Add(ro4);	
	
	tgX = 465;
	tgY = 513;	
	
	targetGroupTopLeft = new elementgroup(0);
	targetGroupTopLeft->SetEventName((orxCHAR*)"TARGET_TOP_LEFT_GROUP_FILLED");
	
	targettopelement *tte1 = new targettopelement(400);
	tte1->SetActivatorPosition(tgX, tgY);
	targetGroupTopLeft->Add(tte1);
	
	targettopelement *tte2 = new targettopelement(400);
	tte2->SetActivatorPosition(tgX+41, tgY-8);
	targetGroupTopLeft->Add(tte2);
	
	targettopelement *tte3 = new targettopelement(400);
	tte3->SetActivatorPosition(tgX+82, 513-14);
	targetGroupTopLeft->Add(tte3);
	
	tgX = 653;
	tgY = 534;	
	
	targetGroupTopRight = new elementgroup(0);
	targetGroupTopRight->SetEventName((orxCHAR*)"TARGET_TOP_RIGHT_GROUP_FILLED");
	
	targettopelement *tte4 = new targettopelement(400);
	tte4->SetActivatorPosition(tgX, tgY);
	targetGroupTopRight->Add(tte4);
	
	targettopelement *tte5 = new targettopelement(400);
	tte5->SetActivatorPosition(tgX+41, tgY+6); 
	targetGroupTopRight->Add(tte5);
	
	targettopelement *tte6 = new targettopelement(400);
	tte6->SetActivatorPosition(tgX+82, tgY+14);
	targetGroupTopRight->Add(tte6);
	
	
	spinner = new spinnerelement(1000, (orxCHAR*)"SPINNER_UP", (orxCHAR*)"SPINNER_DOWN");
	
	laneelement *lane1 = new laneelement();
	//lane1->SetActivatorPosition(325, 66);
	lane1->SetActivatorPosition(485, 66);
	
	laneelement *lane2 = new laneelement();
	//lane2->SetActivatorPosition(420, 80);
	lane2->SetActivatorPosition(580, 80);
	
	laneelement *lane3 = new laneelement();
	//lane3->SetActivatorPosition(515, 94);
	lane3->SetActivatorPosition(675, 94);
	
	//orxObject_CreateFromConfig("TopLeftTargetBracketObject");
	//orxObject_CreateFromConfig("TopRightTargetBracketObject");
	
	leftShield = new shieldelement((orxCHAR*)"ShieldObject", (orxCHAR*)"ShieldIdleAnim", (orxCHAR*)"ShieldPopAndRetractAnim", (orxCHAR*)"ShieldArrowObject");
	//leftShield->SetActivatorPosition(76, 1107);
	leftShield->SetActivatorPosition(236, 1107);
	//leftShield->SetLightPosition(75, 1208);
	leftShield->SetLightPosition(235, 1208);

	rightShield = new shieldelement((orxCHAR*)"ShieldRightObject", (orxCHAR*)"ShieldRightIdleAnim", (orxCHAR*)"ShieldRightPopAndRetractAnim", (orxCHAR*)"ShieldArrowObject");
	//rightShield->SetActivatorPosition(794, 1107);	
	rightShield->SetActivatorPosition(954, 1107);	
	//rightShield->SetLightPosition(793, 1208);
	rightShield->SetLightPosition(953, 1208);
	
	indicatorLights = new indicators();
	
	targetGroupTopLeftFilled = orxFALSE;
	targetGroupTopRightFilled = orxFALSE;
	
	clawMode = 0;
	//orxLOG("clawMode %d", clawMode);
	
	displayedScore = 0;
		
	PinballBase::Init();
	
	indicatorLights->ParentTo(tableObject);
	
	MakeElementAChildOfTheTable(be1); 	
	MakeElementAChildOfTheTable(be2); 	
	MakeElementAChildOfTheTable(be3); 	
	MakeElementAChildOfTheTable(be4); 	
	MakeElementAChildOfTheTable(sle); 	
	MakeElementAChildOfTheTable(sre); 	
	MakeElementAChildOfTheTable(lane1); 	
	MakeElementAChildOfTheTable(lane2); 	
	MakeElementAChildOfTheTable(lane3); 	
	MakeElementAChildOfTheTable(leftClaw); 	
	MakeElementAChildOfTheTable(rightClaw); 	
	MakeElementAChildOfTheTable(topClaw); 	
	MakeElementAChildOfTheTable(te5); 	
	MakeElementAChildOfTheTable(te6); 	
	MakeElementAChildOfTheTable(te7); 	
	MakeElementAChildOfTheTable(ro1); 	
	MakeElementAChildOfTheTable(ro2); 	
	MakeElementAChildOfTheTable(ro3); 	
	MakeElementAChildOfTheTable(ro4); 	
	
	MakeElementAChildOfTheTable(tte1); 	
	MakeElementAChildOfTheTable(tte2); 	
	MakeElementAChildOfTheTable(tte3); 	
	
	MakeElementAChildOfTheTable(tte4); 	
	MakeElementAChildOfTheTable(tte5); 	
	MakeElementAChildOfTheTable(tte6); 	
	
	MakeElementAChildOfTheTable(spinner); 	

	MakeElementAChildOfTheTable(leftShield); 	
	MakeElementAChildOfTheTable(rightShield); 	
	
	
}


void RasterBlaster::MakeElementAChildOfTheTable(element *e){
	if (e == orxNULL){
		return;
	}
	
	if (e->GetActivator() != orxNULL){
		orxObject_SetParent(e->GetActivator(), tableObject);
	}
	
	if (e->GetLight() != orxNULL){
		orxObject_SetParent(e->GetLight(), tableObject);
	}

}

orxSTATUS orxFASTCALL RasterBlaster::AnimationEventHandler(const orxEVENT *_pstEvent){
	
	if (_pstEvent->eType == orxEVENT_TYPE_ANIM){

		orxANIM_EVENT_PAYLOAD *pstPayload;
		pstPayload = (orxANIM_EVENT_PAYLOAD *)_pstEvent->pstPayload;
		const orxSTRING animName = 	pstPayload->zAnimName;
		
		const orxSTRING recipient = orxObject_GetName(orxOBJECT(_pstEvent->hRecipient));
		
		if ( orxString_Compare(        recipient, "SpinnerObject"        )   == 0 ){
			//orxLOG(" -- START Spinnerobject %s", animName); //ADD WHEN EVENT STARTS ONLY!!!!
		}
		
		
		if (_pstEvent->eID == orxANIM_EVENT_START ){
			if ( orxString_Compare(        animName, "SpinnerAnim"        )   == 0 || 
				 orxString_Compare(        animName, "SpinnerReverseAnim"        )   == 0 
			){
				PlaySoundOn( spinner->GetActivator(), (orxCHAR*)"SpinnerEffect" );
			}
			
			if ( orxString_Compare(        animName, "SpinnerIdle"        )   == 0 
			){
				PlaySoundOff(spinner->GetActivator(), (orxCHAR*)"SpinnerEffect");
			}
		}
		
		if (_pstEvent->eID == orxANIM_EVENT_LOOP ){
			
			if ( orxString_Compare(        animName, "SpinnerAnim"        )   == 0 ){
				//orxLOG("--orxANIM_EVENT_LOOP %s", animName); //ADD WHEN EVENT STARTS ONLY!!!!
				
				if (!IsGameOver()){
					AddToScore(600);
				}
				spinner->SlowDownTheSpinner();
				orxFLOAT volumePercent = GetSoundVolumePercent(spinner->GetActivator());
				
			}
			
			if ( orxString_Compare(        animName, "SpinnerReverseAnim"        )   == 0 ){
				//orxLOG("REVERSE --orxANIM_EVENT_LOOP %s", animName); //ADD WHEN EVENT STARTS ONLY!!!!
				
				if (!IsGameOver()){
					AddToScore(600);
				}
				spinner->SlowDownTheSpinner();
				orxFLOAT volumePercent = GetSoundVolumePercent(spinner->GetActivator());
				if (volumePercent > 10) {
					volumePercent -= 10;
					SetSoundPitchAndVolumePercent(spinner->GetActivator(), volumePercent);
				}
			}
			
		}
		
		if (_pstEvent->eID == orxANIM_EVENT_STOP ){
			
			if ( orxString_Compare(        animName, "LeftClawCompressAnim"        )   == 0  ||
				 orxString_Compare(        animName, "RightClawCompressAnim"        )   == 0  ||
				 orxString_Compare(        animName, "TopClawCompressAnim"        )   == 0 
			){
				
				clawelement *claw = static_cast<clawelement *>(orxObject_GetUserData(orxOBJECT(_pstEvent->hRecipient)));
				if (claw->BallIsCaptured()){

					AddToScore(15000);

					if (this->AllClawsFilled() == orxTRUE){
						indicatorLights->AddToBonus(10000);
						this->LaunchMultiBall();
					} else {
						orxLOG("CreateBall %s", animName);
						CreateBall(orxTRUE);
					}
				} else { //no ball captured, and if top, shoot ball back out.
					if (orxString_Compare(        animName, "TopClawCompressAndDecompressAnim"        )   == 0 ){
						//orxVECTOR ballDirection = {-100,0,0};
						//orxObject_SetSpeed(ballObject, &ballDirection); //not bothering, just change the angle of the mesh
					}
				}
			}
			
			
			if ( orxString_Compare(        animName, "SpinnerAnim"        )   == 0 || 
				 orxString_Compare(        animName, "SpinnerReverseAnim"        )   == 0 
			){
				PlaySoundOff(spinner->GetActivator(), (orxCHAR*)"SpinnerEffect");
			}
			
			
		}
		
		if (_pstEvent->eID == orxANIM_EVENT_CUSTOM_EVENT ){  
 
			if ( orxString_Compare(pstPayload->stCustom.zName, "CLAW_MOVE"        )   == 0 ){
 
				clawelement *claw = static_cast<clawelement *>(orxObject_GetUserData(orxOBJECT(_pstEvent->hRecipient)));
				orxOBJECT *capturedBall = claw->GetCapturedBall();
	 
				if (capturedBall != orxNULL){
					orxVECTOR ballInClawVector;
					orxObject_GetPosition(capturedBall, &ballInClawVector);
					
					orxSTRING clawType = claw->GetClawType();
					
					if (orxString_Compare(        clawType, "LEFT"        )   == 0){
						ballInClawVector.fY = 585 - pstPayload->stCustom.fValue;
					} else if(orxString_Compare(        clawType, "RIGHT"        )   == 0){
						ballInClawVector.fX = 715 - pstPayload->stCustom.fValue;
					} else {
						ballInClawVector.fX = 722 + pstPayload->stCustom.fValue;
					}
					orxObject_SetPosition(capturedBall, &ballInClawVector);
				}
 
			}
		}
		
	}

	return orxSTATUS_SUCCESS;
}


orxSTATUS orxFASTCALL RasterBlaster::PhysicsEventHandler(const orxEVENT *_pstEvent) {
	


	
	if (_pstEvent->eType == orxEVENT_TYPE_PHYSICS){
	
		orxPHYSICS_EVENT_PAYLOAD *pstPayload;
		pstPayload = (orxPHYSICS_EVENT_PAYLOAD *)_pstEvent->pstPayload;
		
		if(_pstEvent->eID == orxPHYSICS_EVENT_CONTACT_ADD)
		{
			orxOBJECT *pstRecipientObject, *pstSenderObject;

			/* Gets colliding objects */
			pstRecipientObject = orxOBJECT(_pstEvent->hRecipient);
			pstSenderObject = orxOBJECT(_pstEvent->hSender);

			if (orxString_Compare(orxObject_GetName(pstSenderObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstRecipientObject), "BumperObject") == 0 
			){
				orxVECTOR hitVector = pstPayload->vNormal;
				ProcessBallAndBumperCollision(pstSenderObject, pstRecipientObject, hitVector);
				
				CreateSpanglesAtObject(pstSenderObject, (orxCHAR*)"MainParticleObject");				
			} 	

			if (orxString_Compare(orxObject_GetName(pstRecipientObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstSenderObject), "BumperObject") == 0 
			){
				orxVECTOR hitVector = FlipVector(pstPayload->vNormal);
				ProcessBallAndBumperCollision(pstRecipientObject, pstSenderObject, hitVector);
				
				CreateSpanglesAtObject(pstRecipientObject, (orxCHAR*)"MainParticleObject");
			} 		

			if (orxString_Compare(orxObject_GetName(pstSenderObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstRecipientObject), (orxCHAR*)"SlingShotLeftObject") == 0 
			){
				ProcessBallAndLeftSlingShotCollision(pstSenderObject, pstRecipientObject);
				
				CreateSpanglesAtObject(pstSenderObject, (orxCHAR*)"LighterMainParticleObject");				
				
			} 	

			if (orxString_Compare(orxObject_GetName(pstRecipientObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstSenderObject), "SlingShotLeftObject") == 0 
			){
				ProcessBallAndLeftSlingShotCollision(pstRecipientObject, pstSenderObject);
				
				CreateSpanglesAtObject(pstRecipientObject, (orxCHAR*)"LighterMainParticleObject");				
			} 	
			
			if (orxString_Compare(orxObject_GetName(pstSenderObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstRecipientObject), "SlingShotRightObject") == 0 
			){
				ProcessBallAndRightSlingShotCollision(pstSenderObject, pstRecipientObject);
				
				CreateSpanglesAtObject(pstSenderObject, (orxCHAR*)"LighterMainParticleObject");				
			} 	

			if (orxString_Compare(orxObject_GetName(pstRecipientObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstSenderObject), "SlingShotRightObject") == 0 
			){
				ProcessBallAndRightSlingShotCollision(pstRecipientObject, pstSenderObject);
				
				CreateSpanglesAtObject(pstRecipientObject, (orxCHAR*)"LighterMainParticleObject");				
			} 
			
			if (orxString_Compare(orxObject_GetName(pstSenderObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstRecipientObject), "TargetRightObject") == 0 
			){
				ProcessBallAndTargetRightCollision(pstSenderObject, pstRecipientObject);
			} 	

			if (orxString_Compare(orxObject_GetName(pstRecipientObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstSenderObject), "TargetRightObject") == 0 
			){
				ProcessBallAndTargetRightCollision(pstRecipientObject, pstSenderObject);
			} 
			
			if (orxString_Compare(orxObject_GetName(pstSenderObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstRecipientObject), "TargetTopObject") == 0 
			){
				if (orxString_Compare(pstPayload->zRecipientPartName, "TargetTopBodyPart") == 0){
					ProcessBallAndTargetTopCollision(pstSenderObject, pstRecipientObject);
				}
			} 	

			if (orxString_Compare(orxObject_GetName(pstRecipientObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstSenderObject), "TargetTopObject") == 0 
			){
				if (orxString_Compare(pstPayload->zSenderPartName, "TargetTopBodyPart") == 0){
					ProcessBallAndTargetTopCollision(pstRecipientObject, pstSenderObject);
				}
			} 
			
			if (orxString_Compare(orxObject_GetName(pstRecipientObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstSenderObject), "BallCatcherObject") == 0 
			){
				DestroyBallAndCreateNewBall(pstRecipientObject);
				//leftShield->TurnShieldOff();
				//rightShield->TurnShieldOff();
				
			} 			
			
			if (orxString_Compare(orxObject_GetName(pstSenderObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstRecipientObject), "BallCatcherObject") == 0 
			){
				DestroyBallAndCreateNewBall(pstSenderObject);
				//leftShield->TurnShieldOff();
				//rightShield->TurnShieldOff();
				
			} 
			
			if (orxString_Compare(orxObject_GetName(pstRecipientObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstSenderObject), "RolloverLightObject") == 0 
			){
				ProcessBallAndRolloverLightCollision(pstSenderObject);
			} 			
			
			if (orxString_Compare(orxObject_GetName(pstSenderObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstRecipientObject), "RolloverLightObject") == 0 
			){
				ProcessBallAndRolloverLightCollision(pstRecipientObject);
			} 
			
			if (orxString_Compare(orxObject_GetName(pstRecipientObject), "BallObject") == 0
			&&
				(orxString_Compare(orxObject_GetName(pstSenderObject), "DirectionRollOverObject") == 0 ||
				orxString_Compare(orxObject_GetName(pstSenderObject), "DirectionRollOverCloserObject") == 0 )
			){
				ProcessBallAndDirectionRolloverCollision(pstSenderObject, (orxSTRING)pstPayload->zSenderPartName);
			} 			
			
			if (orxString_Compare(orxObject_GetName(pstSenderObject), "BallObject") == 0
			&&
				(orxString_Compare(orxObject_GetName(pstRecipientObject), "DirectionRollOverObject") == 0 ||
				orxString_Compare(orxObject_GetName(pstRecipientObject), "DirectionRollOverCloserObject") == 0  )
			){
				ProcessBallAndDirectionRolloverCollision(pstRecipientObject, (orxSTRING)pstPayload->zRecipientPartName);
			} 
			
			if (orxString_Compare(orxObject_GetName(pstRecipientObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstSenderObject), "TableObject") == 0 
			){
				CreateSpanglesAtObject(pstRecipientObject, (orxCHAR*)"SparkleParticle");
			} 			
			
			if (orxString_Compare(orxObject_GetName(pstSenderObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstRecipientObject), "TableObject") == 0 
			){
				CreateSpanglesAtObject(pstSenderObject, (orxCHAR*)"SparkleParticle");
			} 
			
			if (orxString_Compare(orxObject_GetName(pstRecipientObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstSenderObject), "TrapObject") == 0 
			){
				if (GetABallObjectIntheChannel() != orxNULL){
					SetLauncherTrap(orxFALSE);
				}
			} 			
			
			if (orxString_Compare(orxObject_GetName(pstSenderObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstRecipientObject), "TrapObject") == 0 
			){
				if (GetABallObjectIntheChannel() != orxNULL){
					SetLauncherTrap(orxFALSE);
				}
			} 
			
			
			if (orxString_Compare(orxObject_GetName(pstRecipientObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstSenderObject), "SpinnerObject") == 0 
			){
				spinnerelement *spin = static_cast<spinnerelement *>(orxObject_GetUserData(pstSenderObject));
				spin->RegisterHit((orxSTRING)pstPayload->zSenderPartName);
			} 			
			
			if (orxString_Compare(orxObject_GetName(pstSenderObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstRecipientObject), "SpinnerObject") == 0 
			){
				spinnerelement *spin = static_cast<spinnerelement *>(orxObject_GetUserData(pstRecipientObject));
				spin->RegisterHit((orxSTRING)pstPayload->zRecipientPartName);
			} 
			
			if (orxString_Compare(orxObject_GetName(pstRecipientObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstSenderObject), "LeftClawObject") == 0 
			){
				ProcessBallAndClawCollision(pstRecipientObject, pstSenderObject);
			} 			
			
			if (orxString_Compare(orxObject_GetName(pstSenderObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstRecipientObject), "LeftClawObject") == 0 
			){
				ProcessBallAndClawCollision(pstSenderObject, pstRecipientObject);
			} 
			
			
			if (orxString_Compare(orxObject_GetName(pstRecipientObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstSenderObject), "RightClawObject") == 0 
			){
				ProcessBallAndClawCollision(pstRecipientObject, pstSenderObject);
			} 			
			
			if (orxString_Compare(orxObject_GetName(pstSenderObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstRecipientObject), "RightClawObject") == 0 
			){
				ProcessBallAndClawCollision(pstSenderObject, pstRecipientObject);
			} 
			
			if (orxString_Compare(orxObject_GetName(pstRecipientObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstSenderObject), "TopClawObject") == 0 
			){
				ProcessBallAndClawCollision(pstRecipientObject, pstSenderObject);
			} 			
			
			if (orxString_Compare(orxObject_GetName(pstSenderObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstRecipientObject), "TopClawObject") == 0 
			){
				ProcessBallAndClawCollision(pstSenderObject, pstRecipientObject);
			} 
			
			
			if (orxString_Compare(orxObject_GetName(pstRecipientObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstSenderObject), "ShieldObject") == 0 
			){
				shieldelement *shield = static_cast<shieldelement *>(orxObject_GetUserData(pstSenderObject));
				shield->RegisterHit();
				ProcessBallAndShieldCollision(pstRecipientObject, pstSenderObject);
			} 			
			
			if (orxString_Compare(orxObject_GetName(pstSenderObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstRecipientObject), "ShieldObject") == 0 
			){
				shieldelement *shield = static_cast<shieldelement *>(orxObject_GetUserData(pstRecipientObject));
				shield->RegisterHit();
				ProcessBallAndShieldCollision(pstSenderObject, pstRecipientObject);
			} 
			
			if (orxString_Compare(orxObject_GetName(pstRecipientObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstSenderObject), "ShieldRightObject") == 0 
			){
				shieldelement *shield = static_cast<shieldelement *>(orxObject_GetUserData(pstSenderObject));
				shield->RegisterHit();
				ProcessBallAndShieldCollision(pstRecipientObject, pstSenderObject);
			} 			
			
			if (orxString_Compare(orxObject_GetName(pstSenderObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstRecipientObject), "ShieldRightObject                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               ") == 0 
			){
				shieldelement *shield = (shieldelement *)orxObject_GetUserData(pstRecipientObject);
				shield->RegisterHit();
				ProcessBallAndShieldCollision(pstSenderObject, pstRecipientObject);
			} 
			
		
		}
	}
	
	return orxSTATUS_SUCCESS;
}


/* Updates the displayed score to catch up with the real score. */
void orxFASTCALL RasterBlaster::ScoreUpdater(const orxCLOCK_INFO *_pstClockInfo, void *_pstContext)
{
	
	const int SCORE_COUNTDOWN_STEPS = 250;

	if (!IsGameOver()){
		if (displayedScore < score){
			displayedScore += 100;
			if (displayedScore > score || (score - displayedScore < 100) ){
				displayedScore = score;
			}
			PrintScore(displayedScore);
		}
	}
	
	/* Wind down multipliers and bonus to the score. */
	if (IsGameOver() && !IsTitleVisible()){
		
		if (indicatorLights->bonus > 0){
			orxLOG("bonus is still > 0: %d, displayscore: %d", indicatorLights->bonus, displayedScore);
			orxLOG("and score is: score %d", score);
			
			indicatorLights->SnapshotBonus();
			PlaySoundIfNotAlreadyPlaying(tableObject, "BonusEffect");
			
			int bonusFragment = 0;
			if (indicatorLights->bonus - SCORE_COUNTDOWN_STEPS > 0){
				bonusFragment = SCORE_COUNTDOWN_STEPS;
			} else {
				bonusFragment = indicatorLights->bonus; //remaining fragment less than 100
			}
			
			indicatorLights->bonus = indicatorLights->bonus - bonusFragment;
			score += bonusFragment;
			
			if (indicatorLights->bonus < SCORE_COUNTDOWN_STEPS){
				score += indicatorLights->bonus;
				indicatorLights->bonus = 0;
			}
			
			/* section for bringing down multiplier value */
			orxFLOAT percentOfBonusRemaining = ((float)indicatorLights->bonus /(float)indicatorLights->bonusSnapshotAtGameOver) * 100;
			orxFLOAT percentToSwitchDownWith = 100 - (100 / (float)indicatorLights->GetMultiplier());
			if ((int)percentOfBonusRemaining == (int)percentToSwitchDownWith){
				int multiplier = indicatorLights->GetMultiplier();
				multiplier--;
				indicatorLights->SetMultiplier(multiplier);
			}
			
			
			if (displayedScore < score){
				displayedScore += SCORE_COUNTDOWN_STEPS;
				if (displayedScore > score || (score - displayedScore < SCORE_COUNTDOWN_STEPS) ){
					PlaySoundOff(tableObject, "BonusEffect");
					
					displayedScore = score; //final set in case there is a slight discrepancy.
				}
				PrintScore(displayedScore);
			}
			
			if (indicatorLights->bonus == 0){
				PlaySoundOff(tableObject, "BonusEffect");
			}
			
			
		} else { //bonus done, but if display is still behind, continue to cycle..
			
			
			//orxLOG("bonus is 0: %d, displayscore: %d", indicatorLights->bonus, displayedScore);
			//orxLOG("and score is: score %d", score);
			
			
			if (displayedScore < score){
				PlaySoundIfNotAlreadyPlaying(tableObject, "BonusEffect");
				
				displayedScore += SCORE_COUNTDOWN_STEPS;
				if (displayedScore > score || (score - displayedScore < SCORE_COUNTDOWN_STEPS) ){
					displayedScore = score;
					
					showTitleDelay = 3;
					PlaySoundOff(tableObject, "BonusEffect");
					
					displayedScore = score; //final set in case there is a slight discrepancy.
					PrintScore(displayedScore);
					
					TryUpdateHighscore();

				}
				PrintScore(displayedScore);
			} else {
				if (showTitleDelay == -1){
					orxLOG("FIRE");
					PlaySoundOff(tableObject, "BonusEffect");
					showTitleDelay = 3;
					
					TryUpdateHighscore();
					
				}
			}
			 
		}
	}
		
	
	#ifdef __orxDEBUG__
		
	if (autoPlayMode == orxTRUE && IsGameOver() == orxFALSE){
		
		orxOBJECT *ballLeftBehind = GetABallObjectAtThePlunger();
		if (ballLeftBehind != orxNULL) {
			plung->SetPlungerStrength(50);
			LaunchBall();
		}
		//if (ballInPlay == orxFALSE && 
		if (rightFlipperPressed == orxTRUE) {
			leftFlipperPressed = orxTRUE;
			rightFlipperPressed = orxFALSE;
			StrikeLeftFlipper();
		} else {
			rightFlipperPressed = orxTRUE;
			leftFlipperPressed = orxFALSE;
			StrikeRightFlipper();
		}	
	}
	
	if (autoPlayMode == orxTRUE && IsGameOver() && IsTitleVisible() == orxTRUE){
		StartNewGame();
	}
	
	#endif // __orxDEBUG__	
	
	
}


/* Occurs every second and works for delayed or periodic running of tasks. */
void orxFASTCALL RasterBlaster::SecondsUpdate(const orxCLOCK_INFO *_pstClockInfo, void *_pstContext)
{
	
	secondsSinceTheLastTilt++;
	if (secondsSinceTheLastTilt > 10)
		totalTiltPenalties = 0; //clear penalties if 10 seconds goes by,
	
	if (freeShieldSeconds > 0){
		freeShieldSeconds--;	
	} else {
		
		if (protectLeftShield == orxFALSE && leftShield->IsShieldOn()){
			leftShield->TurnShieldOff();
		}
		
		if (protectRightShield == orxFALSE && rightShield->IsShieldOn()){
			rightShield->TurnShieldOff();
		}
		
	}
	


	PinballBase::SecondsUpdate(_pstClockInfo, _pstContext);

}

orxSTATUS orxFASTCALL RasterBlaster::CustomEventHandler(const orxEVENT *_pstEvent) {
	orxSTRING eventName = (orxCHAR*)_pstEvent->pstPayload;
	
	if (orxString_Compare(eventName, "BALL_LAUNCHED") == 0) {
		leftShield->TurnShieldOn();
		rightShield->TurnShieldOn();
		
		freeShieldSeconds = FREE_SHIELD_SECONDS;
		protectLeftShield = orxFALSE;
		protectRightShield = orxFALSE;
		
	}	
	
	if (orxString_Compare(eventName, "TARGET_TOP_LEFT_GROUP_FILLED") == 0 ){
		//indicatorLights->Flash5();
		leftShield->TurnShieldOn();
		protectLeftShield = orxTRUE;
		
		if (targetGroupTopRightFilled == orxTRUE){
			if (targetGroupTopLeft->ActivatorsOnCount() == 3){
				targetGroupTopLeftFilled = orxFALSE;
				targetGroupTopRightFilled = orxFALSE;
				targetGroupTopLeft->TurnAllActivatorsOff();
				targetGroupTopRight->TurnAllActivatorsOff();
				
				AddToScore(5000);
				indicatorLights->AddToBonus(5000);
				
				clawMode++;
				//orxLOG("clawMode %d", clawMode);
				if (clawMode == 1){
					leftClaw->ActivateClaw();
					rightClaw->ActivateClaw();
				}
				if (clawMode == 2){
					leftClaw->ActivateClaw();
					rightClaw->ActivateClaw();
					topClaw->ActivateClaw();
					
					clawMode = 0; //reset for next time.
					//orxLOG("clawMode %d", clawMode);
				}
			}
		} else {
			targetGroupTopLeftFilled = orxTRUE;
		}
		
		PlaySoundOn(tableSoundCentrePosition, (orxCHAR*)"RightTargetFilledEffect");
	}
	
	if (orxString_Compare(eventName, "TARGET_TOP_RIGHT_GROUP_FILLED") == 0){
		rightShield->TurnShieldOn();
		protectRightShield = orxTRUE;
		
		if (targetGroupTopLeftFilled == orxTRUE){
			if (targetGroupTopRight->ActivatorsOnCount() == 3){
				targetGroupTopLeftFilled = orxFALSE;
				targetGroupTopRightFilled = orxFALSE;
				targetGroupTopLeft->TurnAllActivatorsOff();
				targetGroupTopRight->TurnAllActivatorsOff();
				
				AddToScore(5000);
				indicatorLights->AddToBonus(5000);
				
				clawMode++;
				//orxLOG("clawMode %d", clawMode);
				if (clawMode == 1){
					leftClaw->ActivateClaw();
					rightClaw->ActivateClaw();
				}
				if (clawMode == 2){
					leftClaw->ActivateClaw();
					rightClaw->ActivateClaw();
					topClaw->ActivateClaw();
					
					clawMode = 0; //reset for next time.
					//orxLOG("clawMode %d", clawMode);
				}
			}
		} else {
			targetGroupTopRightFilled = orxTRUE;
		}
		
		PlaySoundOn(tableSoundCentrePosition, (orxCHAR*)"GroupLitEffect");
	}
	
	if (orxString_Compare(eventName, "TARGET_RIGHT_GROUP_FILLED") == 0 ){
		if (indicatorLights->IsROn() == orxTRUE && indicatorLights->IsBOn() == orxFALSE){
			indicatorLights->AddToBonus(10000);
		}
		
		indicatorLights->FlashB();
		
		leftShield->TurnShieldOn();
		protectLeftShield = orxTRUE;
		
		rightShield->TurnShieldOn();
		protectRightShield = orxTRUE;
		
		targetGroupRight->TurnAllActivatorsOff();
		
		PlaySoundOn(tableSoundCentrePosition, (orxCHAR*)"GroupLitEffect");
	}
	
	if (orxString_Compare(eventName, "ROLLOVER_GROUP_FILLED") == 0){
		if (indicatorLights->IsROn() == orxFALSE && indicatorLights->IsBOn() == orxTRUE){
			indicatorLights->AddToBonus(10000);
		}
		
		indicatorLights->FlashR();
		
		if (indicatorLights->IsExtraBallPending()){
			indicatorLights->FlashExtraBall();
			indicatorLights->SetExtraBallPending(orxFALSE);
			balls++;
			PrintBallCount();
		} else {
			int multiplier = indicatorLights->GetMultiplier();
			multiplier++;
			indicatorLights->SetMultiplier(multiplier);
		}
		
		rolloverlightgroup->TurnAllActivatorsOff();
		//AddToScoreAndUpdate(10000); defined in the group
		PlaySoundOn(tableSoundCentrePosition, (orxCHAR*)"GroupLitEffect");
	}
	
	
	
	PinballBase::CustomEventHandler(_pstEvent);
	
	return orxSTATUS_SUCCESS;
}


/*
orxSTATUS orxFASTCALL RasterBlaster::TextureLoadingEventHandler(const orxEVENT *_pstEvent) {
	
	orxLOG("TextureLoadingEventHandler START");
	
	orxU32 texturesRemainingCount = orxTexture_GetLoadCounter();
	orxLOG("texturesRemainingCount in TextureLoadingEventHandler(): %d", texturesRemainingCount);
	
	if (_pstEvent->eType == orxEVENT_TYPE_TEXTURE){
		
		orxTEXTURE *pstSenderObject;
		pstSenderObject = orxTEXTURE(_pstEvent->hSender);
			
		const orxSTRING name = orxTexture_GetName(pstSenderObject);
		
		if (_pstEvent->eID == orxTEXTURE_EVENT_CREATE){
			orxLOG("orxTEXTURE_EVENT_CREATE for %s", name);
		}
		
		if (_pstEvent->eID == orxTEXTURE_EVENT_DELETE){
			orxLOG("orxTEXTURE_EVENT_DELETE for %s", name);
		}
		
		if (_pstEvent->eID == orxTEXTURE_EVENT_NUMBER){
			orxLOG("orxTEXTURE_EVENT_NUMBER for %s", name);
		}	
		
		if (_pstEvent->eID == orxTEXTURE_EVENT_NONE){
			orxLOG("orxTEXTURE_EVENT_NONE for %s", name);
		}	
		
		if (_pstEvent->eID == orxTEXTURE_EVENT_LOAD){
			
			orxLOG("orxTEXTURE_EVENT_LOAD %s loaded.", name);
			
		}
	}
	
	orxLOG("TextureLoadingEventHandler END");
}
 * */


orxSTATUS orxFASTCALL RasterBlaster::TextureLoadingEventHandler(const orxEVENT *_pstEvent) {
	
	if (endTextureLoad == orxTRUE){
		return orxSTATUS_SUCCESS;
	}
	
	orxU32 texturesRemainingCount = orxTexture_GetLoadCounter();
	orxLOG("texturesRemainingCount in TextureLoadingEventHandler(): %d", texturesRemainingCount);
	
	if (texturesRemainingCount > texturesToLoadMax){
		texturesToLoadMax = texturesRemainingCount; //if more have been added, update the max to ensure progress bar is good.
	}
	
	if (_pstEvent->eType == orxEVENT_TYPE_TEXTURE){
		
		if (_pstEvent->eID == orxTEXTURE_EVENT_CREATE){
			orxLOG("orxTEXTURE_EVENT_CREATE");
		}
		
		if (_pstEvent->eID == orxTEXTURE_EVENT_NUMBER){
			orxLOG("orxTEXTURE_EVENT_NUMBER");
		}	
		
		if (_pstEvent->eID == orxTEXTURE_EVENT_LOAD){
			orxLOG("orxTEXTURE_EVENT_LOAD");
			
			orxTEXTURE *pstSenderObject;
			pstSenderObject = orxTEXTURE(_pstEvent->hSender);
			
			const orxSTRING name = orxTexture_GetName(pstSenderObject);
			
			orxFLOAT percent = 100 - (((orxFLOAT)texturesRemainingCount / (orxFLOAT)texturesToLoadMax) * 100);
			SetProgressPercent(percent);
			//orxLOG("Texture %s loaded percent %f, textures to load %d", name, percent, texturesToLoadCount);
			
			if (texturesRemainingCount <= 1){
				endTextureLoad = orxTRUE;
				
				//orxLOG("100 percent loaded. Time to Fade out.");
				
				orxObject_AddFX(loadingProgressObject, "LoadingPageFadeOutFXSlot");	
				orxObject_AddFX(loadingPageObject, "LoadingPageFadeOutFXSlot");	
				orxOBJECT *decal = orxOBJECT(orxObject_GetChild(loadingPageObject));
				if (decal != orxNULL){
					orxObject_AddFX(decal, "LoadingPageFadeOutFXSlot");	
				}
				
				if (loadingProgressObject != orxNULL){
					orxObject_SetLifeTime(loadingProgressObject, 2); 
				}
				if (decal != orxNULL){
					orxObject_SetLifeTime(decal, 2); 
				}
				if (loadingPageObject != orxNULL){
					orxObject_SetLifeTime(loadingPageObject, 2); 
				}
				
			}
			
		}
	}
	
}


orxSTATUS orxFASTCALL RasterBlaster::InputEventHandler(const orxEVENT *_pstEvent){
	
	orxINPUT_EVENT_PAYLOAD *payLoad;
	payLoad = (orxINPUT_EVENT_PAYLOAD *)_pstEvent->pstPayload;
	
	if (_pstEvent->eID == orxINPUT_EVENT_ON){
	
		#ifdef __orxDEBUG__
		
		if (IsEqualTo(payLoad->zInputName, "TestKey")){
			/* test features on/off*/
			leftClaw->ActivateClaw();
			rightClaw->ActivateClaw();
			topClaw->ActivateClaw();
			/* end test */
		}
		
		if (IsEqualTo(payLoad->zInputName, "TestKey2")){
			/* test features on/off*/
			if (cheatMode == orxFALSE){
				cheatMode = orxTRUE;
			} else {
				cheatMode = orxFALSE;
				SetCheatMode(cheatMode);
			}
			//autoPlayMode = orxTRUE;
			/* end test */
		}
		
		#endif // __orxDEBUG__		
		
		
	}
	
	if (_pstEvent->eID == orxINPUT_EVENT_OFF){

		
	}
			
	PinballBase::InputEventHandler(_pstEvent);

	return orxSTATUS_SUCCESS;
}

void RasterBlaster::PrintScore(){
	PrintScore(score);
}

//123456
void RasterBlaster::PrintScore(int overriddenScor){
	//orxLOG("PrintScore +");
	std::stringstream scoreStream;
	std::stringstream formattedScoreStream;
	scoreStream << overriddenScor;
	std::string str = scoreStream.str();
	
	for (int x=str.length()-1; x>=0; x--){
		orxCHAR cc = (orxCHAR)str[x];
		formattedScoreStream << cc;
	}
	
	int length = str.length();
	int paddingLength = 6 - length;
	std::string paddingString = scorePadding.substr(0, paddingLength);

	str = paddingString + str;
	orxSTRING s = (orxCHAR*)str.c_str();
	
	orxObject_SetTextString(scoreObject, s);
	//orxLOG("PrintScore -");
	
}

void RasterBlaster::TurnOffAllMultiplierLights()
{
	indicatorLights->TurnAllMultiplierLightsOff();
}

void RasterBlaster::TurnOffAllTableLights()
{
	rolloverlightgroup->TurnAllActivatorsOff();
	//rolloverlightgroup->TurnAllLightsOff();
	targetGroupRight->TurnAllActivatorsOff();
	//targetGroupRight->TurnAllLightsOff();
	targetGroupTopLeft->TurnAllActivatorsOff();
	targetGroupTopRight->TurnAllActivatorsOff();
	
	indicatorLights->TurnAllLightsOff();
}

void RasterBlaster::ProcessEventsBasedOnScore() 
{
	//mute base implementation
}

void RasterBlaster::ProcessAndroidAccelerometer(){
	//overrider
	//orxLOG("ProcessAndroidAccelerometer ON android: %d", isAndroid);
	
	if (isAndroid == orxFALSE){
		return;
	}
	
	if(  orxMath_Abs(   orxInput_GetValue("AccelX") - lastAccel.fX   ) > 5){
		Tilt();
	}

	if(  orxMath_Abs(   orxInput_GetValue("AccelY") - lastAccel.fY   ) > 5){
		Tilt();
	}

	if(  orxMath_Abs(   orxInput_GetValue("AccelZ") - lastAccel.fZ   ) > 8){
		Tilt();
	}

	orxVector_Set(&lastAccel, orxInput_GetValue("AccelX"), orxInput_GetValue("AccelY"), orxInput_GetValue("AccelZ"));
  
  
	//orxLOG("ProcessAndroidAccelerometer OFF");
}

void RasterBlaster::Tilt(){
	orxLOG("Tilt Fired.");
	
	if (GetABallObjectAtThePlunger() != orxNULL){
		return;
	}
	
	/* Needs to be 2 seconds between bumps */
	if (secondsSinceTheLastTilt <= 2) {
		return;
	}
	
	orxU32 fxNumber = orxMath_GetRandomU32(1, 4);
	//orxLOG("%d", fxNumber);
	
	std::stringstream fxStream;
	fxStream << "FX" << fxNumber << "BumpTable";
	std::string str = fxStream.str();
	
	orxObject_AddFX(tableObject, str.c_str());	
	
	secondsSinceTheLastTilt = 0;
	totalTiltPenalties++;
	if (HasTableBeenTilted()){
		indicatorLights->FlashTilt();
	}
}

void RasterBlaster::ProcessBallAndRightSlingShotCollision(orxOBJECT *ballObject, orxOBJECT *rightSlingShotObject){
	//orxLOG("ProcessBallAndRightSlingShotCollision");
	slingshotelement *sr = (slingshotelement *)orxObject_GetUserData(rightSlingShotObject);
	AddToScore(sr->RegisterHitAsFX());
	
	orxVECTOR slingVector;
	slingVector.fX = -slingShotVector.fX;
	slingVector.fY = slingShotVector.fY;
	slingVector = VaryVector(slingVector, SLINGSHOT_VARIANCE);
	//orxLOG("vector speed %f, %f", slingVector.fX, slingVector.fY);
	
	orxObject_SetSpeed(ballObject, &slingVector);
	
	PlaySoundOn(rightSlingShotObject, (orxCHAR*)"SlingShotEffect"); //tableObject
}

void RasterBlaster::ProcessBallAndLeftSlingShotCollision(orxOBJECT *ballObject, orxOBJECT *leftSlingShotObject){

	slingshotelement *sl = (slingshotelement *)orxObject_GetUserData(leftSlingShotObject);
	AddToScore(sl->RegisterHitAsFX());
	
	orxVECTOR slingVector;
	slingVector.fX = slingShotVector.fX;
	slingVector.fY = slingShotVector.fY;
	slingVector = VaryVector(slingVector, SLINGSHOT_VARIANCE);
	
	orxObject_SetSpeed(ballObject, &slingVector);
	
	PlaySoundOn(leftSlingShotObject, (orxCHAR*)"SlingShotEffect"); //tableObject
}

void RasterBlaster::ProcessBallAndShieldCollision(orxOBJECT *ballObject, orxOBJECT *shieldObject){
	shieldelement *se = (shieldelement *)orxObject_GetUserData(shieldObject);
	
	if (se->IsShieldOn() == orxFALSE){
		return;
	}
	
	AddToScore(se->RegisterHit());
	
	orxVECTOR shieldShootVector;
	shieldShootVector.fY = -1200;
	
	if (orxString_Compare(orxObject_GetName(shieldObject), "ShieldRightObject") == 0){
		shieldShootVector.fX = -30;
		//shieldShootVector = VaryVector(shieldShootVector, 10); //or try vary Y
	} else {
		shieldShootVector.fX = 0;
		shieldShootVector = VaryVector(shieldShootVector, 10); //or try vary Y
	}
	
	orxObject_SetSpeed(ballObject, &shieldShootVector);
	
	PlaySoundOn(shieldObject, (orxCHAR*)"ShieldEffect"); //tableObject
}


void RasterBlaster::ProcessBallAndClawCollision(orxOBJECT *ballObject, orxOBJECT *clawObject){
	cheatMode = orxFALSE;
	SetCheatMode(cheatMode);
	
	clawelement *claw = (clawelement *)orxObject_GetUserData(clawObject);
	
	/* if ball already captured, flip the vector direction and bounce back off. Don't compress the claw. */
	if (claw->BallIsCaptured()){
		orxVECTOR ballDirection = {0,0,0};
		orxObject_GetSpeed(ballObject, &ballDirection);
		ballDirection = FlipVector(ballDirection);
		orxObject_SetSpeed(ballObject, &ballDirection);
		
		return;
	}
	
	/* No ball, and claw is active, capture the ball and compress the claw */
	if (claw->ClawIsActive()){
		PlaySoundOn(tableSoundCentrePosition, (orxCHAR*)"ClawEnteredEffect"); //tableObject
		claw->DeactivateClaw();
		claw->SetCapturedBall(ballObject);
		claw->CompressClaw();
	} else {
	
		/* compress claw if there is no ball captured*/
		claw->CompressClaw();
		PlaySoundOn(tableSoundCentrePosition, (orxCHAR*)"BallCapturedEffect"); //tableObject
		
	}
	
}

void RasterBlaster::ProcessBallAndTargetTopCollision(orxOBJECT *ballObject, orxOBJECT *targetTopObject){
	targettopelement *te = (targettopelement *)orxObject_GetUserData(targetTopObject);
	AddToScore(te->RegisterHit());
	
	PlaySoundOn(tableSoundCentrePosition, (orxCHAR*)"ElementEffect");
}

void RasterBlaster::ProcessBallAndTargetRightCollision(orxOBJECT *ballObject, orxOBJECT *targetRightObject){
	targetrightelement *te = (targetrightelement *)orxObject_GetUserData(targetRightObject);
	AddToScore(te->RegisterHit());
	
	PlaySoundOn(tableSoundCentrePosition, (orxCHAR*)"ElementEffect");
}

void RasterBlaster::LaunchMultiBall(){
	multiBalls = 2; //2 plus 1 in play
	
	if (leftClaw->BallIsCaptured()){
		leftClaw->DropCapturedBall();
	}
	
	if (rightClaw->BallIsCaptured()){
		rightClaw->DropCapturedBall();
	}
	
	if (topClaw->BallIsCaptured()){
		topClaw->DropCapturedBall();
	}
	
	clawMode = 0;
}


void RasterBlaster::DestroyBall(orxOBJECT *ballObject)
{
	orxObject_SetLifeTime(ballObject, 0);

	if (multiBalls == 0) {
		ballInPlay = orxFALSE;
		leftShield->TurnShieldOff();
		rightShield->TurnShieldOff();
	} else if (multiBalls > 0) {

	}

	PlaySoundOn(tableSoundCentrePosition, (orxCHAR*)"BallLostEffect");

}


void RasterBlaster::DestroyBallAndCreateNewBall(orxOBJECT *ballObject)
{
	PinballBase::DestroyBallAndCreateNewBall(ballObject);
	
	if (IsGameOver() == orxFALSE) {
		indicatorLights->ResetAll();
	}
}

orxBOOL RasterBlaster::AllClawsFilled(){
	if (leftClaw->BallIsCaptured() && rightClaw->BallIsCaptured() && topClaw->BallIsCaptured()){
		return orxTRUE;
	}
	
	return orxFALSE;
}


void RasterBlaster::AddToScore(int points)
{
	score += (points * GetMultiplier());
	
	if (score > 999999) {
		score = 0;
		displayedScore = 0;
		balls += 2;
		PrintBallCount();
	}
}


//only works if no balls and nothing in play
void RasterBlaster::StartNewGame()
{
	if (IsGameOver() == orxTRUE) {
		HideTitle();
		orxClock_Pause(pstDancingLightsClock);

		PlaySoundOn(tableSoundCentrePosition, "StartGameEffect");

		score = 0;
		displayedScore = 0;
		clawMode = 0;
		//orxLOG("clawMode %d", clawMode);
		
		PrintScore();
		balls = 5;
		PrintBallCount();

		TurnOffAllTableLights();
		indicatorLights->SetMultiplier(1);
		
		leftClaw->DeactivateClaw();
		rightClaw->DeactivateClaw();
		topClaw->DeactivateClaw();

		leftClaw->DestroyCapturedBall();
		rightClaw->DestroyCapturedBall();
		topClaw->DestroyCapturedBall();

		CreateBall();
		orxLOG("CreateBall StartNewGame");
	}
	
	PinballBase::StartNewGame();
}


