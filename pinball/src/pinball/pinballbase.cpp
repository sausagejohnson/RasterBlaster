#include "orx.h"
#include "elementgroup.h"
#include "element.h"
#include "bumperelement.h"
#include "slingshotelement.h"
#include "targettopelement.h"
#include "targetrightelement.h"
#include "targetrightelement.h"
#include "rolloverlightelement.h"
#include "directionrolloverelement.h"
#include "plunger.h"
#include "pinballbase.h"
#include <sstream>
#include <vector>

const float PinballBase::DRAG_DISTANCE_MAX = 310.0f;

PinballBase::PinballBase()
{
	ballObject = NULL;
	
	touchDownVector = orxVECTOR_0; //The location where a touch occurs.
	touchUpVector = orxVECTOR_0; //The location where a touch is lifted.

	bumperStrength = 1200;
	slingShotVector = {1600, -200, 0};
	flipperRotationSteps = 0.2;
	plungerStrengthMultiplier = 30;
	
	if (scorePadding.length() == 0)
		scorePadding = "000,000,000";

	TOUCH_WIDTH = 140;
	TOUCH_HEIGHT = 140;
	TOUCH_LEFT_X = 41;
	TOUCH_Y = 553;
	TOUCH_RIGHT_X = 378;

	touchTotalFingersDown = 0;

	nativeScreenWidth = 0;
	nativeScreenHeight = 0;

	score = 0;
	balls = 0;
	multiBalls = 0;

	/* Delayed function parameters */
	//trapTimer = 0;
	showTitleDelay = -1;
	zeroOutScoreDelay = -1;

	//texturesToLoadCount = 0;
	endTextureLoad = orxFALSE;
	
	cheatMode = orxFALSE;
	autoPlayMode = orxFALSE;
	secondsSinceTheLastTilt = 60; //set high, no penalty yet.
	totalTiltPenalties = 0; //only two penalties allowed, then tilt is tripped.
	
	dancingLightIndex = 0;
}

PinballBase* PinballBase::_instance = NULL;


PinballBase* PinballBase::instance()
{
	if (_instance == NULL) {
	}
	return _instance;
}

void PinballBase::create()
{
	if (_instance == NULL) {
		_instance = new PinballBase();
	} 
//	else {
//		int no_instance = 1;
//	}
}


orxBOOL PinballBase::IsEqualTo(const orxSTRING stringA, const orxSTRING stringB)
{
	if (orxString_Compare(stringA, stringB) == 0) {
		return orxTRUE;
	}

	return orxFALSE;
}


/* Occurs every quarter second and it used to dance the lights during game over. */
void orxFASTCALL PinballBase::DancingLightsUpdate(const orxCLOCK_INFO *_pstClockInfo, void *_pstContext)
{
	if (rolloverlightgroup->ActivatorsOnCount() == 0)
		rolloverlightgroup->TurnActivatorOnAt(0); //turn on first to get things cycling
	if (targetGroupRight->ActivatorsOnCount() == 0)
		targetGroupRight->TurnActivatorOnAt(0);

	if (dancingLightIndex == 0){
		//light up an external light
	}
	
	if (dancingLightIndex == 3){
		//turn off external light
	}
	
	dancingLightIndex++;
	
	if (dancingLightIndex > 5){
		dancingLightIndex = 0;
	}

	rolloverlightgroup->ShiftActivatorsRight();
	//targetGroupLeft->ShiftLightsRight();
	targetGroupRight->ShiftLightsRight();
/*	if (zeroOutScoreDelay <= 0) {
		if (score == 0) {
			score = 8;
		}
		if (score >= 999999999) {
			score = GetSavedHighScore();
			zeroOutScoreDelay = 5;
		}
		PrintScore();
		score *= 10;
	}*/
	
	int m = GetMultiplier();

	if (m >= 6 ) {
		TurnOffAllMultiplierLights();
		return;
	}
	IncreaseMultiplier();


}

void orxFASTCALL PinballBase::DancingLightsUpdateDispatcher(const orxCLOCK_INFO *_pstClockInfo, void *_pstContext)
{
	_instance->DancingLightsUpdate(_pstClockInfo, _pstContext);
}
void orxFASTCALL PinballBase::SecondsUpdateDispatcher(const orxCLOCK_INFO *_pstClockInfo, void *_pstContext)
{
	_instance->SecondsUpdate(_pstClockInfo, _pstContext);
}
void orxFASTCALL PinballBase::ScoreUpdaterDispatcher(const orxCLOCK_INFO *_pstClockInfo, void *_pstContext)
{
	_instance->ScoreUpdater(_pstClockInfo, _pstContext);
}
orxSTATUS orxFASTCALL PinballBase::CustomEventHandlerDispatcher(const orxEVENT *_pstEvent)
{
	return _instance->CustomEventHandler(_pstEvent);
}
orxSTATUS orxFASTCALL PinballBase::TextureLoadingEventHandlerDispatcher(const orxEVENT *_pstEvent)
{
	return _instance->TextureLoadingEventHandler(_pstEvent);
}
orxSTATUS orxFASTCALL PinballBase::EventHandlerDispatcher(const orxEVENT *_pstEvent)
{
	return _instance->EventHandler(_pstEvent);
}
orxSTATUS orxFASTCALL PinballBase::InputEventHandlerDispatcher(const orxEVENT *_pstEvent)
{
	return _instance->InputEventHandler(_pstEvent);
}
orxSTATUS orxFASTCALL PinballBase::AnimationEventHandlerDispatcher(const orxEVENT *_pstEvent)
{
	return _instance->AnimationEventHandler(_pstEvent);
}
orxSTATUS orxFASTCALL PinballBase::PhysicsEventHandlerDispatcher(const orxEVENT *_pstEvent)
{
	return _instance->PhysicsEventHandler(_pstEvent);
}
void orxFASTCALL PinballBase::UpdateDispatcher(const orxCLOCK_INFO *_pstClockInfo, void *_pstContext)
{
	_instance->Update(_pstClockInfo, _pstContext);
}
orxBOOL orxFASTCALL PinballBase::SaveHighScoreToConfigFileCallbackDispatcher(const orxSTRING _zSectionName, const orxSTRING _zKeyName, const orxSTRING _zFileName, orxBOOL _bUseEncryption)
{
	return _instance->SaveHighScoreToConfigFileCallback(  _zSectionName, _zKeyName, _zFileName, _bUseEncryption);
}


/* Updates the displayed score to catch up with the real score. Muted here. */
void orxFASTCALL PinballBase::ScoreUpdater(const orxCLOCK_INFO *_pstClockInfo, void *_pstContext)
{
}

/* Occurs every second and works for delayed or periodic running of tasks. */
void orxFASTCALL PinballBase::SecondsUpdate(const orxCLOCK_INFO *_pstClockInfo, void *_pstContext)
{

	//if (trapTimer > 0) {
		//trapTimer--;
		//orxLOG("Trap countdown: %d", trapTimer);
	//} else { //if (trapTimer == 1)
		//SetLeftTrap(orxFALSE);
		//SetRightTrap(orxFALSE);
		//trapTimer = 0;
	//}
	


	/* for showing title at the end of a game */
	if (IsGameOver()){
		if (showTitleDelay > 0 ) {
			showTitleDelay--;
			//orxLOG("PinballBase showTitleDelay countdown: %d", showTitleDelay);
		} else if (showTitleDelay == 0) {
			showTitleDelay = -1;
			ShowTitle();
			TurnOffAllTableLights();
			orxClock_Restart(pstDancingLightsClock);
			orxClock_Unpause(pstDancingLightsClock);
		}

		/* for when to zero out the score at the end of a game */
/*		if (zeroOutScoreDelay > 0 ) {
			zeroOutScoreDelay--;
			//orxLOG("showTitleDelay countdown: %d", showTitleDelay);
		} else if (zeroOutScoreDelay == 0) {
			zeroOutScoreDelay = -1;
			score = 0;
		}*/
	}

	/* Multiball mode. Keep trying to create and launch balls until there are 5. */
/*	if (launchMultiBalls == orxTRUE) {
		CreateBall();
		plung->SetPlungerStrength(50);
		LaunchBall();
		orxLOG("How many multiballs %d", multiBalls);
		if (multiBalls > 3) {
			launchMultiBalls = orxFALSE;
		}
	}*/

	/* After multiballs all loaded, any extras left in the channel need to be launched */
/*	if (launchMultiBalls == orxFALSE && multiBalls > 0 ) {
		orxOBJECT *ballLeftBehind = GetABallObjectAtThePlunger();
		if (ballLeftBehind != orxNULL) {
			plung->SetPlungerStrength(50);
			LaunchBall();
		}
	}*/

}

void PinballBase::ShowTitle()
{

	titleObject = orxObject_CreateFromConfig("TitleObject");

	if (isAndroid) {
		orxObject_AddTimeLineTrack(titleObject, "TitleTrackMobile");
	} else {
		orxObject_AddTimeLineTrack(titleObject, "TitleTrackDesktop");
	}
	orxObject_AddTimeLineTrack(titleObject, "ThemeTrack");

}

void PinballBase::HideTitle()
{

	if (titleObject != orxNULL){
		orxObject_SetLifeTime(titleObject, orxFLOAT_0);
	}

}


orxBOOL PinballBase::IsObjectInitialised(orxOBJECT *object){
	if (object == orxNULL){
		return orxFALSE;
	}
	
/*	if (orxSTRUCTURE(object)->u64GUID == orxSTRUCTURE_GUID_MAGIC_TAG_DELETED){ //this appears to crash
		orxSTRUCTURE *stru = orxSTRUCTURE(object);
		return orxFALSE;
	}
	
	if (orxSTRUCTURE(object)->u64GUID == orxU64_UNDEFINED){
		return orxFALSE;
	}*/
	
	return orxTRUE;
}


void PinballBase::PlaySoundOn(orxOBJECT *object, orxSTRING soundFromConfig)
{
	if (!IsObjectInitialised(object)) {
		orxLOG("For some reason, this object is null.");
		return;
	}
	
	orxSOUND *currentSound = orxObject_GetLastAddedSound(object);
	if (currentSound != orxNULL){
		orxObject_RemoveSound(object, soundFromConfig);
	}
	orxObject_AddSound(object, soundFromConfig);
	orxLOG("Sound %s played on object %s", soundFromConfig, orxObject_GetName(object));
}

void PinballBase::PlaySoundIfNotAlreadyPlaying(orxOBJECT *object, orxSTRING soundFromConfig){
	if (!IsObjectInitialised(object)) {
		orxLOG("For some reason, this object is null.");
		return;
	}
	
	orxSOUND *currentSound = orxObject_GetLastAddedSound(object);
	if (currentSound != orxNULL && IsEqualTo(soundFromConfig, orxSound_GetName(currentSound) ) ){
		return;
	}
	orxObject_AddSound(object, soundFromConfig);	
}


void PinballBase::PlaySoundOff(orxOBJECT *object, orxSTRING soundFromConfig)
{
	if (!IsObjectInitialised(object)) {
		orxLOG("For some reason, this object is null.");
		return;
	}

	orxObject_RemoveSound(object, soundFromConfig);
}

orxFLOAT PinballBase::GetSoundVolumePercent(orxOBJECT *object)
{
	if (!IsObjectInitialised(object)) {
		orxLOG("For some reason, this object is null.");
		return 0;
	}
	orxSOUND *sound = orxObject_GetLastAddedSound(object);
	if (sound == orxNULL) {
		return 0;
	}
	orxFLOAT volume = orxSound_GetVolume(sound);
	return (volume) * 100;
}

void PinballBase::SetSoundPitchAndVolumePercent(orxOBJECT *object, orxFLOAT percent)
{
	if (!IsObjectInitialised(object)) {
		orxLOG("For some reason, this object is null.");
		return;
	}
	orxSOUND *sound = orxObject_GetLastAddedSound(object);
	if (sound == orxNULL) {
		return;
	}

	orxFLOAT fraction = percent/100;
	
	orxSound_SetVolume(sound, fraction);
	orxSound_SetPitch(sound, fraction);
}

orxSTATUS orxFASTCALL PinballBase::CustomEventHandler(const orxEVENT *_pstEvent)
{
	orxSTRING eventName = (orxCHAR*)_pstEvent->pstPayload;

	if (orxString_Compare(eventName, "BALL_LAUNCHED") == 0) {
		SetLauncherTrap(orxTRUE);
		//SetLeftTrap(orxTRUE);
		//SetRightTrap(orxTRUE);
		//trapTimer = trapMaxTime * GetMultiplier();
		//PlaySoundOn(ballObject, (orxCHAR*)"LaunchTrapCloseEffect");
	}

	if (orxString_Compare(eventName, "TARGET_LEFT_GROUP_FILLED") == 0 ||
	    orxString_Compare(eventName, "TARGET_RIGHT_GROUP_FILLED") == 0 ||
	    orxString_Compare(eventName, "ROLLOVER_GROUP_FILLED") == 0) {
		IncreaseMultiplier();
		//SetLeftTrap(orxTRUE);
		//SetRightTrap(orxTRUE);
		//trapTimer = trapMaxTime * GetMultiplier();
		PlaySoundOn(tableObject, (orxCHAR*)"GroupLitEffect");
	}

	if (orxString_Compare(eventName, "TARGET_RIGHT_GROUP_FILLED") == 0)
		targetGroupRightFilled = orxTRUE;

	if (orxString_Compare(eventName, "ROLLOVER_GROUP_FILLED") == 0)
		rollOversFilled = orxTRUE;

	if (targetGroupRightFilled == orxTRUE && rollOversFilled == orxTRUE) {
		targetGroupRightFilled = orxFALSE;
		rollOversFilled = orxFALSE;
		//launchMultiBalls = orxTRUE;
	}

	return orxSTATUS_SUCCESS;
}



void PinballBase::DestroyBallAndCreateNewBall(orxOBJECT *ballObject)
{
	DestroyBall(ballObject);

	launchMultiBalls = orxFALSE; //lose a ball during multiball launches and they turn off.

	if (multiBalls > 0) {
		multiBalls--;
	}

	totalTiltPenalties = 0; //clear any wrong doing :)

	//orxLOG("Multiballs: %d, balls %d", multiBalls, balls);

	if (IsGameOver() == orxTRUE) {
		//showTitleDelay = 1;
		//zeroOutScoreDelay = 5;
	} else {
		TurnOffAllMultiplierLights();
	}
	if (multiBalls == 0 && ballInPlay == orxFALSE) {
		orxLOG("multiBalls == 0 && ballInPlay == orxFALSE");
		CreateBall();
	}
}


orxSTATUS orxFASTCALL PinballBase::TextureLoadingEventHandler(const orxEVENT *_pstEvent)
{


	/*if (endTextureLoad == orxTRUE) {
		return orxSTATUS_SUCCESS;
	}



	if (_pstEvent->eType == orxEVENT_TYPE_TEXTURE) {

		if (_pstEvent->eID == orxTEXTURE_EVENT_CREATE) {
			//orxLOG("orxTEXTURE_EVENT_CREATE");
		}

		if (_pstEvent->eID == orxTEXTURE_EVENT_NUMBER) {
			//orxLOG("orxTEXTURE_EVENT_NUMBER");
		}

		if (_pstEvent->eID == orxTEXTURE_EVENT_LOAD) {
			//orxTIMELINE_EVENT_PAYLOAD *pstPayload;
			//pstPayload = (orxTIMELINE_EVENT_PAYLOAD *)_pstEvent->pstPayload;
			//orxLOG("orxTEXTURE_EVENT_LOAD");

			orxTEXTURE *pstSenderObject;
			pstSenderObject = orxTEXTURE(_pstEvent->hSender);

			const orxSTRING name = orxTexture_GetName(pstSenderObject);

			texturesToLoadCount++;
			orxFLOAT percent = (orxFLOAT)texturesToLoadCount / (orxFLOAT)TEXTURE_TO_LOAD_MAX * 100;
			SetProgressPercent(percent);
			//orxLOG("Texture %s loaded percent %f, textures to load %d", name, percent, texturesToLoadCount);
			//orxLOG("==== Texture %s %s ", pstPayload->zEvent, pstPayload->zTrackName);

			if (percent >= 100 && endTextureLoad == orxFALSE) {
				endTextureLoad = orxTRUE;

				//orxLOG("100 percent loaded. Time to Fade out.");

				orxObject_AddFX(loadingProgressObject, "LoadingPageFadeOutFXSlot");
				orxObject_AddFX(loadingPageObject, "LoadingPageFadeOutFXSlot");
				orxOBJECT *decal = orxOBJECT(orxObject_GetChild(loadingPageObject));
				orxObject_AddFX(decal, "LoadingPageFadeOutFXSlot");

				orxObject_SetLifeTime(loadingProgressObject, 2);
				orxObject_SetLifeTime(decal, 2);
				orxObject_SetLifeTime(loadingPageObject, 2);

			}
		}
	}*/

	return orxSTATUS_SUCCESS;
}


orxSTATUS orxFASTCALL PinballBase::AnimationEventHandler(const orxEVENT *_pstEvent)
{
	return orxSTATUS_SUCCESS;
}

orxSTATUS orxFASTCALL PinballBase::PhysicsEventHandler(const orxEVENT *_pstEvent)
{
	if (_pstEvent->eType == orxEVENT_TYPE_PHYSICS) {

//		orxPHYSICS_EVENT_PAYLOAD *pstPayload;
//		pstPayload = (orxPHYSICS_EVENT_PAYLOAD *)_pstEvent->pstPayload;

		if(_pstEvent->eID == orxPHYSICS_EVENT_CONTACT_ADD) {
			orxOBJECT *pstRecipientObject, *pstSenderObject;

			/* Gets colliding objects */
			//pstRecipientObject = orxOBJECT(_pstEvent->hRecipient);
			//pstSenderObject = orxOBJECT(_pstEvent->hSender);
		}
	}
	
	return orxSTATUS_SUCCESS;
}

orxSTATUS orxFASTCALL PinballBase::InputEventHandler(const orxEVENT *_pstEvent)
{
	if (_pstEvent->eType == orxEVENT_TYPE_INPUT) {

		if (_pstEvent->eID == orxINPUT_EVENT_ON) {

			if (orxMouse_IsButtonPressed(orxMOUSE_BUTTON_LEFT)) {
				orxVECTOR mouseWorldPosition;

				//orxLOG("Mouse position: %f x %f", mouseWorldPosition.fX, mouseWorldPosition.fY);

				orxObject_Enable(rightTouchObject, orxFALSE);
				orxOBJECT *object = orxObject_Pick(&mouseWorldPosition, orxU32_UNDEFINED);

				if (object != orxNULL) {
					const orxSTRING objectName;
					objectName = orxObject_GetName(object);
					//orxLOG("Clicked on object: %s x:%f y:%f z:%f", objectName, mouseWorldPosition.fX, mouseWorldPosition.fY, mouseWorldPosition.fZ);
				}


				orxObject_Enable(rightTouchObject, orxTRUE);

			}
			if (orxInput_IsActive("LeftFlipper") && orxInput_HasNewStatus("LeftFlipper")) {
				if (IsGameOver() == orxFALSE && HasTableBeenTilted() == orxFALSE) {
					leftFlipperPressed = orxTRUE;
					StrikeLeftFlipper();
				}
			}
			if (orxInput_IsActive("RightFlipper") && orxInput_HasNewStatus("RightFlipper")) {
				if (IsGameOver() == orxFALSE && HasTableBeenTilted() == orxFALSE) {
					rightFlipperPressed = orxTRUE;
					StrikeRightFlipper();
				}
			}
			if (orxInput_IsActive("PlayGame") == orxTRUE && orxInput_HasNewStatus("PlayGame") && orxInput_IsActive("Alt") == orxFALSE) {
				//orxLOG("orxInput_IsActive(PlayGame) +");
				StartNewGame();
				//orxLOG("orxInput_IsActive(PlayGame) -");
			}
			// Full screen switch
			if (
			    orxInput_IsActive("PlayGame") == orxTRUE && orxInput_HasNewStatus("PlayGame") &&
			    orxInput_IsActive("Alt") == orxTRUE //&& orxInput_HasNewStatus("Alt")
			) {
				orxBOOL isFullScreen = orxDisplay_IsFullScreen();
				orxDisplay_SetFullScreen (!isFullScreen);

				//orxLOG("FULLSCREEN!!");
			}
			if (orxInput_IsActive("PlungerSmack") && orxInput_HasNewStatus("PlungerSmack")) {
				plung->SetPlungerStrength(53);
				LaunchBall();
			}

			if (orxInput_IsActive("PlungerPull") == orxTRUE && orxInput_HasNewStatus("PlungerPull")) {
				//orxLOG("--------------- PlungerPull KEY ON ------------------");
				plung->plungerIsBeingPulled = orxTRUE;
			}

		}

		if (_pstEvent->eID == orxINPUT_EVENT_OFF) {
			if (orxInput_IsActive("LeftFlipper") == orxFALSE && orxInput_HasNewStatus("LeftFlipper")) {
				PlaySoundOn(leftFlipperObject, (orxCHAR*)"FlipperDownEffect");
				leftFlipperPressed = orxFALSE;
			}
			if (orxInput_IsActive("RightFlipper") == orxFALSE && orxInput_HasNewStatus("RightFlipper")) {
				PlaySoundOn(rightFlipperObject, (orxCHAR*)"FlipperDownEffect");
				rightFlipperPressed = orxFALSE;
			}
			if (orxInput_IsActive("Tilt") == orxFALSE && orxInput_HasNewStatus("Tilt")) {
				if (!IsGameOver()){
					Tilt();
				}
			}
			if (orxInput_IsActive("TestKey") == orxFALSE && orxInput_HasNewStatus("TestKey")) {
			}
			if (orxInput_IsActive("PlungerSmack") == orxFALSE && orxInput_HasNewStatus("PlungerSmack")) {
				plung->RestorePosition();
			}
			if (orxInput_IsActive("PlungerPull") == orxFALSE && orxInput_HasNewStatus("PlungerPull")) {
				//orxLOG("--------------- PlungerPull KEY OFF ------------------");
				LaunchBall();
				plung->plungerIsBeingPulled = orxFALSE;
			}
		}

	}

	return orxSTATUS_SUCCESS;
}

void PinballBase::SetCheatMode(orxBOOL onOff){
	
	#ifndef __orxDEBUG__
		return;
	#endif // __orxDEBUG__
	
	//orxLOG("SetCheatMode to %d", onOff);
	if (!IsObjectInitialised(ballObject))
		return;
		
	if (orxString_Compare(orxObject_GetName(ballObject), "BallObject") != 0) {//probably a bad routine. If the name changes, memory aint good.
		return;
	}

	if (onOff == orxTRUE){
		if (IsObjectInitialised(ballObject)){
			orxVECTOR stopSpeed = {0, 0, 0};
			orxObject_SetCustomGravity (ballObject, &stopSpeed);
			//orxObject_SetSpeed(ballObject, &stopSpeed);
		
			orxVECTOR mousePosition;
			orxVECTOR worldMousePosition;
			
			orxMouse_GetPosition(&mousePosition);
			
			//orxRender_GetWorldPosition(&mousePosition, orxNULL,  &worldMousePosition);
			worldMousePosition = GetAdjustedTouchCoords(mousePosition);
			
			orxObject_SetPosition(ballObject, &worldMousePosition);
		}
	} else {
		if (IsObjectInitialised(ballObject)){
			orxObject_SetCustomGravity (ballObject, orxNULL);
		}
	}
}


/** Physics and Anim Event handler (TODO: break up)
 */
orxSTATUS orxFASTCALL PinballBase::EventHandler(const orxEVENT *_pstEvent)
{
	if (cheatMode == orxTRUE){
		SetCheatMode(cheatMode);
	}

	if (_pstEvent->eType == orxEVENT_TYPE_VIEWPORT) {

		if (_pstEvent->eID == orxVIEWPORT_EVENT_RESIZE) {
			SetFrustumToWhateverDisplaySizeCurrentlyIs();

			orxFLOAT deviceWidth = 865;
			orxFLOAT deviceHeight = 1280;
			orxBOOL getSizeSuccess = orxDisplay_GetScreenSize(&deviceWidth, &deviceHeight);
			if (getSizeSuccess == orxTRUE) {
				SetupTouchZonesValues(deviceWidth, deviceHeight);
				SetTouchHighlightPositions(deviceWidth, deviceHeight);
			}
		}

	}

	if((_pstEvent->eType == orxEVENT_TYPE_RENDER) 
	&& (_pstEvent->eID == orxRENDER_EVENT_OBJECT_START)) 
	{ 
		/*orxRENDER_EVENT_PAYLOAD *pstPayload; 
		orxVECTOR vPos; 

		pstPayload = (orxRENDER_EVENT_PAYLOAD *)_pstEvent->pstPayload; 
		orxVector_Set(&vPos, pstPayload->stObject.pstTransform->fDstX, pstPayload->stObject.pstTransform->fDstY, orxFLOAT_0); 
		//orxDisplay_DrawCircle(&vPos, 50, orx2RGBA(255, 255, 0, 255), orxTRUE); 
		
		orxOBOX ballBoxArea;

		orxVECTOR pivot;
		pivot.fX = 0;
		pivot.fY = 0;
		pivot.fZ = 0;

		orxVECTOR position;
		
		//orxMouse_GetPosition 	( 	&position	);
		//orxLOG("Mouse -- Scren Position x:%f y:%f", position.fX, position.fY);
		
		//orxRender_GetWorldPosition( &position, orxNULL, &position );
		//orxLOG("Mouse -- World Position x:%f y:%f", position.fX, position.fY);
		
		position.fX = 350; //854
		position.fY = 0; //356
		position.fZ = orxFLOAT_0;

		orxVECTOR size;
		size.fX = 84;
		size.fY = 850;
		size.fZ = 1;

		orxOBox_2DSet(&ballBoxArea, &position, &pivot, &size, 0);

		orxRGBA colour;
		colour.u8R = 255;
		colour.u8G = 128;
		colour.u8B = 0;
		colour.u8A = 200;
		orxDisplay_DrawOBox( &ballBoxArea, colour, orxTRUE);	
		
		
		if (GetABallObjectIntheChannel() != orxNULL)
			orxLOG("Object under mouse? Yes x:%f y:%f", position.fX, position.fY);
		else
			orxLOG("Object under mouse? No x:%f y:%f", position.fX, position.fY);*/
			
	} 


	if (_pstEvent->eType == orxEVENT_TYPE_OBJECT) {

		if (_pstEvent->eID == orxOBJECT_EVENT_DELETE) {
			orxOBJECT *pstSenderObject;
			pstSenderObject = orxOBJECT(_pstEvent->hSender);

			if (orxString_Compare(orxObject_GetName(pstSenderObject), "TitleObject") == 0) {
				titleObject = orxNULL; //null so that if HideTitle() is called too quickly, titleObject may not dispose in time.
			}

		}
		
		if (_pstEvent->eID == orxOBJECT_EVENT_CREATE) {
			orxOBJECT *pstSenderObject;
			pstSenderObject = orxOBJECT(_pstEvent->hSender);

			if (orxString_Compare(orxObject_GetName(pstSenderObject), "HighScoreObject") == 0) {
				int lastSaveHighScore = GetSavedHighScore();
				std::stringstream scoreStream;
				std::stringstream formattedScoreStream;
				scoreStream << lastSaveHighScore;
				std::string str = scoreStream.str();
				
				for (int x=str.length()-1; x>=0; x--){
					orxCHAR cc = (orxCHAR)str[x];
					formattedScoreStream << cc;
				}
				
				int length = str.length();
				int paddingLength = 6 - length;
				std::string paddingString = scorePadding.substr(0, paddingLength);

				str = paddingString + str;
				orxSTRING s = (orxCHAR*)str.c_str();
				
				orxObject_SetTextString(pstSenderObject, s);
			}

		}

	}








	if (_pstEvent->eType == orxEVENT_TYPE_SYSTEM) {



		if(_pstEvent->eID == orxSYSTEM_EVENT_TOUCH_BEGIN ) {
			//orxLOG("------------ TOUCH BEGIN");
			orxSYSTEM_EVENT_PAYLOAD *payload;
			payload = (orxSYSTEM_EVENT_PAYLOAD *) _pstEvent->pstPayload;

			touchTotalFingersDown++;
			orxLOG("Fingers Down %d", touchTotalFingersDown);

			/*Single Tap anywhere will restart the game if appropriate*/
			//orxLOG("balls: %d", balls);
			//orxLOG("ballinPlay: %d", ballInPlay);
			//orxLOG("IsGameOver: %d", IsGameOver());
			//orxLOG("IsTitleVisible: %d", IsTitleVisible());
			if (IsGameOver() == orxTRUE && IsTitleVisible() == orxTRUE) {
				//orxLOG("_pstEvent->eID == orxSYSTEM_EVENT_TOUCH_BEGIN +");
				StartNewGame();
				//orxLOG("_pstEvent->eID == orxSYSTEM_EVENT_TOUCH_BEGIN -");
				//return orxSTATUS_SUCCESS; //check that returning here doesn't require clearing previous touch flags
			}



			orxVECTOR localTouchVector = { 0,0,0 };
			localTouchVector.fX = payload->stTouch.fX;
			localTouchVector.fY = payload->stTouch.fY;

			//orxVECTOR touchWorldPosition;
			//touchWorldPosition = GetAdjustedTouchCoords(localTouchVector);

			touchDownVector = localTouchVector; //required for plunger calculation

			orxBOOL leftTouched = IsWithin(localTouchVector.fX, localTouchVector.fY, TOUCH_LEFT_X, TOUCH_Y, TOUCH_LEFT_X + TOUCH_WIDTH, TOUCH_HEIGHT *2);
			orxBOOL rightTouched = IsWithin(localTouchVector.fX, localTouchVector.fY, TOUCH_RIGHT_X, TOUCH_Y, TOUCH_RIGHT_X + TOUCH_WIDTH + 256, TOUCH_HEIGHT *2);
			//orxBOOL swipeTouched = IsWithin(localTouchVector.fX, localTouchVector.fY, 174, 139, 284, 557);
			orxBOOL swipeTouched = IsWithin(localTouchVector.fX, localTouchVector.fY, TOUCH_LEFT_X, 0, TOUCH_RIGHT_X + TOUCH_WIDTH, TOUCH_HEIGHT*2);

			if (leftTouched == orxTRUE && HasTableBeenTilted() == orxFALSE) {
				orxObject_RemoveFX(leftTouchObject , "TouchFadeOutFXSlot");
				orxObject_AddFX(leftTouchObject , "TouchFadeUpFXSlot");
				leftFlipperPressed = orxTRUE;
				StrikeLeftFlipper();
			}

			if (rightTouched == orxTRUE && HasTableBeenTilted() == orxFALSE) {
				orxObject_RemoveFX(rightTouchObject , "TouchFadeOutFXSlot");
				orxObject_AddFX(rightTouchObject , "TouchFadeUpFXSlot");
				rightFlipperPressed = orxTRUE;
				StrikeRightFlipper();
			}

			if (swipeTouched == orxTRUE) {
			}


		} else if(_pstEvent->eID == orxSYSTEM_EVENT_TOUCH_END  ) {


			//orxLOG("------------ TOUCH END ---------------");


			orxSYSTEM_EVENT_PAYLOAD *payload;
			payload = (orxSYSTEM_EVENT_PAYLOAD *) _pstEvent->pstPayload;
			//orxLOG("Location %f x %f", payload->stTouch.fX, payload->stTouch.fY);

			if (touchTotalFingersDown == 10){ //hidden trick. Start cheatmode
				autoPlayMode = orxTRUE;	
			}

			touchTotalFingersDown--;
			orxLOG("Fingers Down %d", touchTotalFingersDown);

			//orxLOG("Mouse up");

			orxVECTOR localTouchVector = { 0,0,0 };
			localTouchVector.fX = payload->stTouch.fX;
			localTouchVector.fY = payload->stTouch.fY;


			//orxVECTOR touchWorldPosition;
			//touchWorldPosition = GetAdjustedTouchCoords(localTouchVector);
			//orxLOG("AdjustedTouchCoords Touch: %f x %f", touchWorldPosition.fX, touchWorldPosition.fY);


			touchUpVector = 	localTouchVector; //required for plunger calc

			orxBOOL leftUnTouched = IsWithin(localTouchVector.fX, localTouchVector.fY, TOUCH_LEFT_X, TOUCH_Y, TOUCH_LEFT_X + TOUCH_WIDTH, TOUCH_HEIGHT*2);
			orxBOOL rightUnTouched = IsWithin(localTouchVector.fX, localTouchVector.fY, TOUCH_RIGHT_X, TOUCH_Y, TOUCH_RIGHT_X + TOUCH_WIDTH + 256, TOUCH_HEIGHT*2);
			//orxBOOL swipeUnTouched = IsWithin(localTouchVector.fX, localTouchVector.fY, 174, 139, 284, 557);
			orxBOOL swipeUnTouched = IsWithin(localTouchVector.fX, localTouchVector.fY, TOUCH_LEFT_X, 0, TOUCH_RIGHT_X + TOUCH_WIDTH, TOUCH_HEIGHT*2);

			if (leftUnTouched == orxTRUE) {
				orxObject_RemoveFX(leftTouchObject , "TouchFadeUpFXSlot");
				orxObject_AddFX(leftTouchObject , "TouchFadeOutFXSlot");
				leftFlipperPressed = orxFALSE;
			}

			if (rightUnTouched == orxTRUE) {
				orxObject_RemoveFX(rightTouchObject , "TouchFadeUpFXSlot");
				orxObject_AddFX(rightTouchObject , "TouchFadeOutFXSlot");
				rightFlipperPressed = orxFALSE;
			}

			if (swipeUnTouched == orxTRUE) {
				LaunchBall();
			} else {
				LaunchBall();
			}

			orxFLOAT dragDistance = orxVector_GetDistance(&touchDownVector, &touchUpVector);
			//orxLOG("Drag distance: %f", dragDistance);
			if (dragDistance > 10 && dragDistance < DRAG_DISTANCE_MAX) {

			}


		} else if(_pstEvent->eID == orxSYSTEM_EVENT_TOUCH_MOVE  ) {

			//orxLOG("--- --- orxSYSTEM_EVENT_TOUCH_MOVE");
			orxSYSTEM_EVENT_PAYLOAD *payload;
			payload = (orxSYSTEM_EVENT_PAYLOAD *) _pstEvent->pstPayload;

			orxVECTOR localTouchVector = { 0,0,0 };
			localTouchVector.fX = payload->stTouch.fX;
			localTouchVector.fY = payload->stTouch.fY;

			//orxVECTOR touchWorldPosition;
			//touchWorldPosition = GetAdjustedTouchCoords(localTouchVector);

			orxBOOL leftMovingWithin = IsWithin(localTouchVector.fX, localTouchVector.fY, TOUCH_LEFT_X, TOUCH_Y, TOUCH_LEFT_X + TOUCH_WIDTH, TOUCH_HEIGHT*2);
			orxBOOL rightMovingWithin = IsWithin(localTouchVector.fX, localTouchVector.fY, TOUCH_RIGHT_X, TOUCH_Y, TOUCH_RIGHT_X + TOUCH_WIDTH + 256, TOUCH_HEIGHT*2);
			//orxBOOL swipeMovingWithin = IsWithin(localTouchVector.fX, localTouchVector.fY, 174, 139, 284, 557);
			//orxBOOL swipeMovingWithin = IsWithin(localTouchVector.fX, localTouchVector.fY, 174, 139, 884, 1257);
			orxBOOL swipeMovingWithin = IsWithin(localTouchVector.fX, localTouchVector.fY, TOUCH_LEFT_X, 0, TOUCH_RIGHT_X + TOUCH_WIDTH, TOUCH_HEIGHT*2);

			/*this fixed moving out of the zone but turns off the other flipper if a new one is pressed, so commenting out...
			if (leftMovingWithin == orxFALSE && leftFlipperPressed == orxTRUE){
				orxObject_RemoveFX(leftTouchObject , "TouchFadeUpFXSlot");
				orxObject_AddFX(leftTouchObject , "TouchFadeOutFXSlot");
				leftFlipperPressed = orxFALSE;
				orxLOG("-----------TOUCH LEFT OFF");
			}

			if (rightMovingWithin == orxFALSE && rightFlipperPressed == orxTRUE){
				orxObject_RemoveFX(rightTouchObject , "TouchFadeUpFXSlot");
				orxObject_AddFX(rightTouchObject , "TouchFadeOutFXSlot");
				rightFlipperPressed = orxFALSE;
			}
			//end of commenting
			*/

			/*			if (swipeMovingWithin == orxFALSE){
							orxCOLOR touchColour;
							orxObject_GetColor(swipeTouchObject, &touchColour);
							if (touchColour.fAlpha == 1){
								orxObject_RemoveFX(swipeTouchObject , "TouchFadeUpFXSlot");
								orxObject_AddFX(swipeTouchObject , "TouchFadeOutFXSlot");
							}
						}*/

			orxFLOAT dragDistance = orxVector_GetDistance(&touchDownVector, &localTouchVector);
			//orxLOG("Move Drag distance: %f", dragDistance);
			if (dragDistance > 10 && dragDistance < DRAG_DISTANCE_MAX) {
				float percent = (dragDistance / DRAG_DISTANCE_MAX) * 100;
				plung->SetPlungerLevelByPercent(percent);
				plung->SetPlungerStrengthByPercent(percent);
				//orxLOG("SetPlungerLevelByPercent %f", percent);
			}
		}
	}


	/* Done! */
	return orxSTATUS_SUCCESS;
}

orxBOOL PinballBase::IsTitleVisible()
{
	return (titleObject != orxNULL);
}

orxBOOL PinballBase::IsGameOver()
{
	if (balls == 0 && multiBalls == 0 && ballInPlay == orxFALSE) {
		return orxTRUE;
	}

	return orxFALSE;
}

orxBOOL PinballBase::HasTableBeenTilted()
{
	if (totalTiltPenalties >= 2){
		return orxTRUE;
	}
	
	return orxFALSE;
}


/* a bug to have to do this???? */
orxVECTOR PinballBase::GetAdjustedTouchCoords(orxVECTOR touchInVector)
{
	//return touchInVector;
	orxVECTOR adjustedTouchWorldPosition;
	orxRender_GetWorldPosition( &touchInVector, orxNULL, &adjustedTouchWorldPosition );

	//adjustedTouchWorldPosition.fY += 162;
	adjustedTouchWorldPosition.fZ = 0;
	return adjustedTouchWorldPosition;
}

orxVECTOR PinballBase::ScreenToWorldCoords(orxVECTOR inVector){
	orxVECTOR convertedPosition;
	orxRender_GetWorldPosition( &inVector, orxNULL, &convertedPosition );
	
	return convertedPosition;
}

orxVECTOR PinballBase::WorldToScreenCoords(orxVECTOR inVector){
	orxVECTOR convertedPosition;
	orxRender_GetScreenPosition( &inVector, orxNULL, &convertedPosition );
	
	return convertedPosition;
}

void PinballBase::TurnOffAllTableLights()
{
	//orxLOG("TurnOffAllTableLights +");
	rolloverlightgroup->TurnAllActivatorsOff();
	rolloverlightgroup->TurnAllLightsOff();
	targetGroupRight->TurnAllActivatorsOff();
	targetGroupRight->TurnAllLightsOff();
	//orxLOG("TurnOffAllTableLights -");
}

//only works if no balls and nothing in play
void PinballBase::StartNewGame()
{
	//orxLOG("StartNewGame +");
}

void PinballBase::StrikeLeftFlipper()
{
	orxObject_SetAngularVelocity(leftFlipperObject, -17.8);
	PlaySoundOn(leftFlipperObject, (orxCHAR*)"FlipperEffect");
}
void PinballBase::StrikeRightFlipper()
{
	rolloverlightgroup->ShiftActivatorsRight();
	orxObject_SetAngularVelocity(rightFlipperObject, 17.8);
	PlaySoundOn(rightFlipperObject, (orxCHAR*)"FlipperEffect");
}

orxBOOL PinballBase::IsWithin(float x, float y, float x1, float y1, float x2, float y2)
{
	if (x >= x1 && x <= x2) {
		if (y >= y1 && y <= y2) {
			return orxTRUE;
		}
	}
	return orxFALSE;
}


void PinballBase::CreateSpanglesAtObject(orxOBJECT *object, orxSTRING particleNameFromConfig)
{
	if (object == orxNULL)
		return;
		
	//orxLOG("Create %s at %s", orxObject_GetName(object), particleNameFromConfig);

	orxVECTOR objectVector;
	orxObject_GetPosition(object, &objectVector);
	objectVector.fZ = -0.1;

	orxOBJECT *splash = orxObject_CreateFromConfig(particleNameFromConfig);
	orxObject_SetPosition(splash, &objectVector);
}

void PinballBase::ProcessBallAndDirectionRolloverCollision(orxOBJECT *directionRollOverObject, orxSTRING collidedBodyPart)
{
	directionrolloverelement *dr = static_cast<directionrolloverelement*>(orxObject_GetUserData(directionRollOverObject));
	if (dr == orxNULL){
		return;
	}	
	
	AddToScore(dr->RegisterHit(collidedBodyPart));
}

void PinballBase::ProcessBallAndRolloverLightCollision(orxOBJECT *rolloverLightObject)
{
	rolloverlightelement *ro = static_cast<rolloverlightelement *>(orxObject_GetUserData(rolloverLightObject));
	AddToScore(ro->RegisterHit());

	//PlaySoundOn(rolloverLightObject, (orxCHAR*)"ElementEffect");
}

void PinballBase::ProcessBallAndTargetTopCollision(orxOBJECT *ballObject, orxOBJECT *targetTopObject)
{
}

void PinballBase::ProcessBallAndTargetRightCollision(orxOBJECT *ballObject, orxOBJECT *targetRightObject)
{
}

void PinballBase::ProcessBallAndRightSlingShotCollision(orxOBJECT *ballObject, orxOBJECT *rightSlingShotObject)
{
	slingshotelement *sr = static_cast<slingshotelement *>(orxObject_GetUserData(rightSlingShotObject));
	if (sr == orxNULL){
		return;
	}	
	
	AddToScore(sr->RegisterHit());

	orxVECTOR slingVector;
	slingVector.fX = -slingShotVector.fX;
	slingVector.fY = slingShotVector.fY;
	slingVector = VaryVector(slingVector, SLINGSHOT_VARIANCE);
	//orxLOG("vector speed %f, %f", slingVector.fX, slingVector.fY);

	orxObject_SetSpeed(ballObject, &slingVector);

	PlaySoundOn(rightSlingShotObject, (orxCHAR*)"SlingShotEffect"); //tableObject
}

void PinballBase::ProcessBallAndLeftSlingShotCollision(orxOBJECT *ballObject, orxOBJECT *leftSlingShotObject)
{
	slingshotelement *sl = static_cast<slingshotelement *>(orxObject_GetUserData(leftSlingShotObject));
	if (sl == orxNULL){
		return;
	}	
	
	AddToScore(sl->RegisterHit());

	orxVECTOR slingVector;
	slingVector.fX = slingShotVector.fX;
	slingVector.fY = slingShotVector.fY;
	slingVector = VaryVector(slingVector, SLINGSHOT_VARIANCE);

	orxObject_SetSpeed(ballObject, &slingVector);

	PlaySoundOn(leftSlingShotObject, (orxCHAR*)"SlingShotEffect"); //tableObject
}

void PinballBase::ProcessBallAndBumperCollision(orxOBJECT *ballObject, orxOBJECT *bumperObject, orxVECTOR hitVector)
{
	bumperelement *be = static_cast<bumperelement *>(orxObject_GetUserData(bumperObject));
	if (be == orxNULL){
		return;
	}	
	
	AddToScore(be->RegisterHit());

	PlaySoundOn(tableSoundCentrePosition, (orxCHAR*)"BumperEffect"); //tableObject

	hitVector.fX = -hitVector.fX * bumperStrength;
	hitVector.fY = -hitVector.fY * bumperStrength;

	orxObject_SetSpeed(ballObject, &hitVector);
}

void PinballBase::CreateBall(){
	CreateBall(orxFALSE);
}


/* Only creates a ball if none is in the channel. */
void PinballBase::CreateBall(orxBOOL supressBallCountReduction)
{
	orxLOG("PinballBase::CreateBall +");

	orxOBJECT *objectUnderPick = GetABallObjectIntheChannel();

	if (objectUnderPick == orxNULL) {
		//const orxSTRING objectStringName;
		//objectStringName = orxObject_GetName(objectUnderPick);

		//if (orxString_Compare(objectStringName, "BallObject") != 0 && IsGameOver() == orxFALSE){
		if (IsGameOver() == orxFALSE) {
			ballObject = orxObject_CreateFromConfig("BallObject");
			PlaySoundOn(ballObject, (orxCHAR*)"BallLaunchEffect");
			//SetRightTrap(orxTRUE);
			SetLauncherTrap(orxFALSE);
			if (launchMultiBalls == orxFALSE) {
				if (supressBallCountReduction == orxFALSE){
					DecreaseBallCount();
				}
			} else {
				if (multiBalls <= 3) { //specific code for bean farmer. Refactor!
					multiBalls++;
				}
			}
			ballInPlay = orxTRUE;
		}
	}

	orxLOG("PinballBase::CreateBall -");

}

/*
 * Multballs must be used up before normal balls.
 */
void PinballBase::DecreaseBallCount()
{
	//orxLOG("PrintBallCount +");
	if (balls > 0) {
		balls--;
		PrintBallCount();
	}
	//orxLOG("PrintBallCount -");
}

void PinballBase::IncreaseBallCount(int bonusScoreMarker)
{
	for (int x=0; x<ballIncreases.size(); x++) {
		int ballIncrease = ballIncreases[x];
		if (bonusScoreMarker == ballIncrease) {
			x = ballIncreases.size();
			return;
		}
	}
	ballIncreases.push_back(bonusScoreMarker);
	balls++;
	PrintBallCount();
	CreateSpanglesAtObject(ballCountObject, (orxCHAR*)"ExtraBallMainParticleObject");
	PlaySoundOn(ballCountObject, (orxCHAR*)"BonusEffect");
}

void PinballBase::PrintBallCount()
{
	std::stringstream ballCountStream;
	ballCountStream << balls;
	std::string ballCountString = ballCountStream.str();
	orxSTRING ballCountOrxString = (orxCHAR*)ballCountString.c_str();
	orxObject_SetTextString(ballCountObject, ballCountOrxString);
}

orxOBJECT* PinballBase::GetABallObjectAtThePlunger()
{

	orxVECTOR ballPickVector;
	ballPickVector.fX = 878;
	ballPickVector.fY = 1185;
	ballPickVector.fZ = -1.0;

	orxOBOX ballBoxArea;

	orxVECTOR pivot;
	pivot.fX = 0;
	pivot.fY = 0;
	pivot.fZ = 0;

	orxVECTOR position;
	position.fX = 854;//854;
	position.fY = 1150;//1100;
	position.fZ = -0.1;

	orxVECTOR size;
	size.fX = 21;
	size.fY = 160;
	size.fZ = 1;

	orxOBox_2DSet(&ballBoxArea, &position, &pivot, &size, 0);

	orxU32 ballGroupID = orxCamera_GetGroupID(pstCamera, 1);

	orxOBJECT *ballToShoot = orxObject_BoxPick(&ballBoxArea, ballGroupID);
	return ballToShoot;

}

orxOBJECT* PinballBase::GetABallObjectIntheChannel()
{

	orxOBOX ballBoxArea;

	orxVECTOR pivot;
	pivot.fX = 0;
	pivot.fY = 0;
	pivot.fZ = 0;

	orxVECTOR position;
	position.fX = 1854;
	position.fY = 356;
	
	//testline
	//orxMouse_GetPosition 	( 	&position	);
	position.fZ = orxFLOAT_0;
	position.fZ = -0.1;

	orxVECTOR size;
	size.fX = 84; //42;
	size.fY = 850;
	size.fZ = 1;

	orxOBox_2DSet(&ballBoxArea, &position, &pivot, &size, 0);

	orxU32 ballGroupID = orxCamera_GetGroupID(pstCamera, 1);

	return orxObject_BoxPick(&ballBoxArea, ballGroupID);;
}

void PinballBase::LaunchBall()
{
	int strength = plung->GetPlungerStrength();
	//orxLOG("strength when launching %d", s);
	if (strength == 0)
		return;

	orxObject_Enable(rightTouchObject, orxFALSE);

	orxVECTOR ballShootVector;
	ballShootVector.fX = 0;
	ballShootVector.fY = -300 - (strength * plungerStrengthMultiplier);
	ballShootVector = VaryVectorY(ballShootVector, PLUNGER_SMACK_VARIANCE);
	//orxLOG("=========== launch ball %d and shootvector %f", plung->GetPlungerStrength(), ballShootVector.fY);


	orxOBJECT *ballToShoot = GetABallObjectAtThePlunger();
	//orxOBJECT *ballToShoot = orxObject_Pick(&ballPickVector, orxU32_UNDEFINED);

	if (ballToShoot != orxNULL) {
		const orxSTRING objectName;
		objectName = orxObject_GetName(ballToShoot);

		if (orxString_Compare(objectName, "BallObject") == 0) {

			orxVECTOR ballShiftUpVector; //so the ball and plunger con't collide on launch
			orxObject_GetPosition(ballToShoot, &ballShiftUpVector);
			ballShiftUpVector.fY = ballShiftUpVector.fY - 100;//50;
			orxObject_SetPosition(ballToShoot, &ballShiftUpVector);
			//orxObject_SetSpeed(ballToShoot, &ballShootVector);

			orxObject_SetSpeed(ballToShoot, &ballShootVector);

			//plung->SetToSmackUpPosition();

			PlaySoundOn(ballToShoot, (orxCHAR*)"BallLaunchEffect"); //tableObject
		} else {
			//orxLOG("COULDNT LOCATE BALL");
		}
	} else {
		//orxLOG("COULDNT LOCATE ANY OBJECT");
	}

	plung->RestorePosition();
	orxObject_Enable(rightTouchObject, orxTRUE);

}

void PinballBase::DestroyBall(orxOBJECT *ballObject)
{
	orxObject_SetLifeTime(ballObject, 0);

	if (multiBalls == 0) {
		ballInPlay = orxFALSE;
	}

	PlaySoundOn(tableSoundCentrePosition, (orxCHAR*)"BallLostEffect");

	//TurnOffAllMultiplierLights();
}


void PinballBase::SetLauncherTrap(orxBOOL close)
{
	launcherTrapOn = close;
	if (launcherTrapOn == orxTRUE) {
		orxObject_SetAngularVelocity(trapObject, 10.4);
	} else {
		orxObject_SetAngularVelocity(trapObject, -10.4);
	}
}
/*void PinballBase::SetLeftTrap(orxBOOL close){
	leftTrapOn = close;
	if (leftTrapOn == orxTRUE){
		orxObject_SetAngularVelocity(trapLeftObject, -20.8);
	}
}
void PinballBase::SetRightTrap(orxBOOL close){
	rightTrapOn = close;
	if (rightTrapOn == orxTRUE){
		orxObject_SetAngularVelocity(trapRightObject, 20.8);
	}
}*/

void PinballBase::AddToScore(int points)
{
	score += (points * GetMultiplier());
}

void PinballBase::AddToScoreAndUpdate(int points)
{
	AddToScore(points);
	PrintScore();
	ProcessEventsBasedOnScore();
}

void PinballBase::ProcessEventsBasedOnScore()
{

	if (score > 10000000) {
		IncreaseBallCount(10000000);
	}
	if (score > 5000000) {
		IncreaseBallCount(5000000);
	}
	if (score > 3000000) {
		IncreaseBallCount(3000000);
	}
	if (score > 2000000) {
		IncreaseBallCount(2000000);
	}
	if (score > 1000000) {
		IncreaseBallCount(1000000);
	}
	if (score > 500000) {
		IncreaseBallCount(500000);
	}

	int multiplier = GetMultiplier();
	if (multiplier == 6) {
		return; //don't bother if on 6. Nothing more player can achieve.
	}

}

void PinballBase::TurnOffAllMultiplierLights()
{
	//int test = 1;
	//orxLOG("TurnOffAllMultiplierLights +");
	/*	orxObject_Enable(x2Object, orxFALSE);
		orxObject_Enable(x3Object, orxFALSE);
		orxObject_Enable(x4Object, orxFALSE);
		orxObject_Enable(x5Object, orxFALSE);
		orxObject_Enable(x6Object, orxFALSE);*/
	//orxLOG("TurnOffAllMultiplierLights -");
}

void PinballBase::IncreaseMultiplier()
{
/*	int multiplier = GetMultiplier();
	if (multiplier < 6)
		multiplier++;

	TurnOffAllMultiplierLights();*/

	/*	switch (multiplier) {
			case 6:
				orxObject_Enable(x6Object, orxTRUE);
			case 5:
				orxObject_Enable(x5Object, orxTRUE);
			case 4:
				orxObject_Enable(x4Object, orxTRUE);
			case 3:
				orxObject_Enable(x3Object, orxTRUE);
			case 2:
				orxObject_Enable(x2Object, orxTRUE);
		}*/

}

int PinballBase::GetMultiplier()
{

	/*	if (orxObject_IsEnabled(x6Object)){
			return 6;
		}
		if (orxObject_IsEnabled(x5Object)){
			return 5;
		}
		if (orxObject_IsEnabled(x4Object)){
			return 4;
		}
		if (orxObject_IsEnabled(x3Object)){
			return 3;
		}
		if (orxObject_IsEnabled(x2Object)){
			return 2;
		}*/

	return 1;
}


void PinballBase::PrintScore(){
	PrintScore(score);
}

//1,123,456
void PinballBase::PrintScore(int overriddenScore)
{
	//orxLOG("PrintScore +");
	std::stringstream scoreStream;
	std::stringstream formattedScoreStream;
	scoreStream << overriddenScore;
	std::string str = scoreStream.str();

	int thousandCounter = 3;
	for (int x=str.length()-1; x>=0; x--) {
		orxCHAR cc = (orxCHAR)str[x];
		formattedScoreStream << cc;
		if (thousandCounter == 0) {
			thousandCounter = 3;
			str.insert(x+1, ",");
		}
		thousandCounter--;
	}

	int length = str.length();
	int paddingLength = 11 - length;
	std::string paddingString = scorePadding.substr(0, paddingLength);

	str = paddingString + str;
	orxSTRING s = (orxCHAR*)str.c_str();

	orxObject_SetTextString(scoreObject, s);
	//orxLOG("PrintScore -");

}

void PinballBase::Tilt(){
	//stub
}


orxVECTOR PinballBase::FlipVector(orxVECTOR vector)
{
	vector.fX = -vector.fX;
	vector.fY = -vector.fY;
	return vector;
}

orxVECTOR PinballBase::VaryVector(orxVECTOR vector, int maxVariance)
{
	orxFLOAT variance = orxMath_GetRandomFloat(-maxVariance/2, maxVariance/2);
	vector.fX = vector.fX + variance;
	vector.fY = vector.fY + variance;

	return vector;
}

orxVECTOR PinballBase::VaryVectorY(orxVECTOR vector, int maxVariance)
{
	orxFLOAT variance = orxMath_GetRandomFloat(-maxVariance/2, maxVariance/2);
	vector.fY = vector.fY + variance;

	return vector;
}



/** Update callback for handling states after keypresses
 */
void orxFASTCALL PinballBase::Update(const orxCLOCK_INFO *_pstClockInfo, void *_pstContext)
{
	//orxLOG("===== Update +");

	if (leftFlipperPressed == orxTRUE) {
		//orxLOG("leftFlipperPressed == orxTRUE");
		orxFLOAT rotation = orxObject_GetRotation(leftFlipperObject);
		if (rotation < -0.9) {
			orxObject_SetAngularVelocity(leftFlipperObject, 0.0);
			orxObject_SetRotation(leftFlipperObject, -0.9);
		}
	}

	if (leftFlipperPressed == orxFALSE) {
		//orxLOG("leftFlipperPressed == orxFALSE");
		orxFLOAT rotation = orxObject_GetRotation(leftFlipperObject);
		orxObject_SetAngularVelocity(leftFlipperObject, 0.0);

		if (rotation < 0) {
			rotation += 0.2;
			orxObject_SetRotation(leftFlipperObject, rotation);
		} else if(rotation > 0) {
			orxObject_SetRotation(leftFlipperObject, 0.0);
		}

	}

	if (rightFlipperPressed == orxTRUE) {
		orxFLOAT rotation = orxObject_GetRotation(rightFlipperObject);
		if (rotation >= 0.9) {
			orxObject_SetAngularVelocity(rightFlipperObject, 0.0);
			orxObject_SetRotation(rightFlipperObject, 0.9);
		}
	}

	if (rightFlipperPressed == orxFALSE) {
		orxFLOAT rotation = orxObject_GetRotation(rightFlipperObject);
		orxObject_SetAngularVelocity(rightFlipperObject, 0.0);

		if (rotation >= 0.2) {
			rotation -= 0.2;
			orxObject_SetRotation(rightFlipperObject, rotation);
		} else if(rotation >= 0) {
			orxObject_SetRotation(rightFlipperObject, 0.0);
		}
	}

	if (launcherTrapOn == orxTRUE) {

		orxFLOAT rotation = orxObject_GetRotation(trapObject);
		if (rotation >= 0.9) {
			orxObject_SetAngularVelocity(trapObject, 0.0);
			orxObject_SetRotation(trapObject, 0.9);
		}
	}
	if (launcherTrapOn == orxFALSE) {
		orxFLOAT rotation = orxObject_GetRotation(trapObject);
		if (rotation <= 0) {
			orxObject_SetAngularVelocity(trapObject, 0.0);
			orxObject_SetRotation(trapObject, 0.0);
		}
	}

	

	if (plung->plungerIsBeingPulled == orxTRUE) {
		plung->PullDown();
	}
	if (plung->plungerIsBeingPulled == orxFALSE) {
	}

	//orxLOG("===== Update -");


}


void PinballBase::SetupTouchZonesValues(orxFLOAT screenWidth, orxFLOAT screenHeight)
{

	TOUCH_WIDTH = screenWidth / 2;
	TOUCH_HEIGHT = screenHeight / 2;
	TOUCH_LEFT_X = 0;
	TOUCH_Y = screenHeight / 2;
	TOUCH_RIGHT_X = (screenWidth / 2); //+256 hack for android, find out whats going on here

}


void PinballBase::SetTouchHighlightPositions(orxFLOAT screenWidth, orxFLOAT screenHeight)
{

	//orxLOG("touch x: %f by %f", screenWidth, screenHeight);



	orxVECTOR leftTouchPosition;
	leftTouchPosition.fX = 0;
	leftTouchPosition.fY = 1280;
	leftTouchPosition.fZ = -0.2;

	orxVECTOR rightTouchPosition;
	rightTouchPosition.fX = 960;
	rightTouchPosition.fY = 1280;
	rightTouchPosition.fZ = -0.2;

	/*orxVECTOR leftTouchPosition;
	leftTouchPosition.fX = 0;
	leftTouchPosition.fY = screenHeight;
	leftTouchPosition.fZ = -0.2;

	orxVECTOR rightTouchPosition;
	rightTouchPosition.fX = screenWidth;
	rightTouchPosition.fY = screenHeight;
	rightTouchPosition.fZ = -0.2;*/

	/*leftTouchPosition = ScreenToWorldCoords(leftTouchPosition);
	rightTouchPosition = ScreenToWorldCoords(rightTouchPosition);

	leftTouchPosition.fY = leftTouchPosition.fY - screenHeight;
	rightTouchPosition.fY = rightTouchPosition.fY - screenHeight;*/


	//orxLOG("y pos x: %f", leftTouchPosition.fY);

	orxObject_SetPosition(leftTouchObject, &leftTouchPosition);
	orxObject_SetPosition(rightTouchObject, &rightTouchPosition);

}


orxFLOAT PinballBase::GetSuitableFrustumWidthFromCurrentDevice()
{
	orxFLOAT deviceWidth = 865; //default
	orxFLOAT deviceHeight = 1280; //default
	orxFLOAT suitableFrustumWidth = deviceWidth;
	orxFLOAT suitableFrustumHeight = deviceHeight;

	orxBOOL getSizeSuccess = orxDisplay_GetScreenSize(&deviceWidth, &deviceHeight);
	if (getSizeSuccess == orxTRUE) {

		//get aspect of the current device resolution or window size:
		orxFLOAT deviceAspect = deviceWidth / deviceHeight;

		suitableFrustumWidth = 1280 * deviceAspect;
	}

	return suitableFrustumWidth;
}

orxFLOAT PinballBase::GetSuitableFrustumHeightFromCurrentDevice()
{
	orxFLOAT deviceWidth = 865;
	orxFLOAT deviceHeight = 1280;
	orxFLOAT suitableFrustumWidth = deviceWidth;
	orxFLOAT suitableFrustumHeight = deviceHeight;

	orxBOOL getSizeSuccess = orxDisplay_GetScreenSize(&deviceWidth, &deviceHeight);
	if (getSizeSuccess == orxTRUE) {

		//get aspect of the current device resolution or window size:
		orxFLOAT deviceAspect = deviceWidth / deviceHeight;

		suitableFrustumHeight = 865 / deviceAspect;
	}

	return suitableFrustumHeight;
}

/* This changes the frustum slightly to show a little decorative edge if the device shows black bars on aspect. */
void PinballBase::SetFrustumToWhateverDisplaySizeCurrentlyIs()
{

	/**
	 * At the end, the frustrum is the thing that changes
	 *
	 * screen is 600 x 800, aspect is: 0.75
	 * frust is 865 x 1280
	 * aspectFrustWidth = 1280 * 0.75 =  960, so potential frustrum is 960 x 1280
	 * //but 960 > 865.
	 * //So 854 will be the frust width
	 * //frust height will be 865 / 0.75 = 1138
	 *
	 *
	 * screen is 380 x 800, aspect is: 0.475
	 * frust is 865 x 1280
	 * aspectFrustWidth = 1280 * 0.75 =  608, so potential frustrum is 608 x 1280
	 * 608 much less than 865 so stick with 865
	 * frustrum height stays at 1280
	 *
	 */

	orxFLOAT deviceWidth = 865; //default
	orxFLOAT deviceHeight = 1280; //default
	orxFLOAT suitableFrustumWidth = deviceWidth;
	orxFLOAT suitableFrustumHeight = deviceHeight;

	orxBOOL getSizeSuccess = orxDisplay_GetScreenSize(&deviceWidth, &deviceHeight);
	if (getSizeSuccess == orxTRUE) {
		suitableFrustumWidth = GetSuitableFrustumWidthFromCurrentDevice();
		suitableFrustumHeight = 1280;

		if (suitableFrustumWidth < 865) {

			suitableFrustumWidth = 865;
			//suitableFrustumHeight = GetSuitableFrustumHeightFromCurrentDevice();

		}

		//orxLOG("SetFrustumToWhateverDisplaySizeCurrentlyIs size: %f x %f", suitableFrustumWidth, suitableFrustumHeight);

		orxCamera_SetFrustum(pstCamera, suitableFrustumWidth, suitableFrustumHeight, 0, 2);
	}
}

void PinballBase::SetProgressPercent(orxFLOAT percent)
{
	if (loadingProgressObject == orxNULL)
		return;
		
	orxFLOAT max = 637;

	if (loadingProgressObject != orxNULL) {
		orxFLOAT pixels = max * percent / 100;
		if (pixels == 0)
			pixels = 1;
		orxVECTOR scale;
		scale.fX = pixels;
		scale.fY = 1;
		scale.fZ = 0;
		orxObject_SetScale(loadingProgressObject, &scale);
	}
}


orxBOOL orxFASTCALL PinballBase::SaveHighScoreToConfigFileCallback(const orxSTRING _zSectionName, const orxSTRING _zKeyName, const orxSTRING _zFileName, orxBOOL _bUseEncryption)
{
	// Return orxTRUE for the section "Save", orxFALSE otherwise
	// -> filters out everything but the "Save" section
	return (orxString_Compare(_zSectionName, "Highscore") == 0) ? orxTRUE : orxFALSE;
}


int PinballBase::GetSavedHighScore()
{
	/* Does the highscore section exist? It may not have been created or loaded. Try loading first. */
	orxBOOL hasIt = orxConfig_HasSection("Highscore");

	if (!orxConfig_HasSection("Highscore")) {
		const orxSTRING zFile = orxFile_GetApplicationSaveDirectory("save.ini"); 
		orxConfig_Load(zFile);
	}

	if(orxConfig_PushSection("Highscore")) {
		const orxU64 highScore = orxConfig_GetU64("Highscore");
		orxConfig_PopSection();

		return highScore;
	}

	return 0;
}


void PinballBase::SaveHighScoreToConfig()
{
	if(orxConfig_PushSection("Highscore")) {
		const orxSTRING platform = orxConfig_GetString("Highscore");

		std::stringstream scoreStream;
		scoreStream << score;
		std::string scoreString = scoreStream.str();
		orxSTRING scoreOrxString = (orxCHAR*)scoreString.c_str();

		orxConfig_SetString("Highscore", scoreOrxString);
		orxConfig_PopSection();

		const orxSTRING zSaveFile = orxFile_GetApplicationSaveDirectory("save.ini"); 
		orxConfig_Save(zSaveFile, orxTRUE, SaveHighScoreToConfigFileCallbackDispatcher);
	}
}

void PinballBase::TryUpdateHighscore()
{
	if (score > GetSavedHighScore()) {
		SaveHighScoreToConfig();
	}
}

void PinballBase::DeterminePlatform()
{

	if(orxConfig_PushSection("Platform")) {
		const orxSTRING platform = orxConfig_GetString("Platform");
		if (orxString_Compare(platform, "Android") == 0) {
			isAndroid = orxTRUE;
		}

		orxConfig_PopSection();
	}

}


void PinballBase::ProcessAndroidAccelerometer(){
	//processing stub for the base class
	//int test = 1;
}



/** Inits the game
 */
void PinballBase::Init()
{
	isAndroid = orxFALSE;
	DeterminePlatform();

	if (isAndroid == orxTRUE){
		orxConfig_Load("rasterblaster/android.ini");
		orxInput_Load(orxNULL); //reload the input overrides to suppress any joystick messages
	}

	loadingPageObject = orxObject_CreateFromConfig("LoadingPageObject"); //loading-screen-dot.png
	loadingProgressObject = orxObject_CreateFromConfig("LoadingPageProgressObject"); //progress-bar.png
	endTextureLoad = orxFALSE; //reset for android restarts
	texturesToLoadMax = orxTexture_GetLoadCounter(); //reset for android restarts, used for percent calculations


	orxEvent_AddHandler(orxEVENT_TYPE_TEXTURE, TextureLoadingEventHandlerDispatcher);
	orxFLOAT deviceWidth = 865;
	orxFLOAT deviceHeight = 1280;

	orxBOOL getSizeSuccess = orxDisplay_GetScreenSize(&deviceWidth, &deviceHeight);
	SetupTouchZonesValues(deviceWidth, deviceHeight);


		


#ifndef __orxDEBUG__
	orxDEBUG_ENABLE_LEVEL(orxDEBUG_LEVEL_LOG, orxFALSE);
#endif // __orxDEBUG__

	orxLOG("=================== INIT ================");

	orxCLOCK       *pstClock;
	orxCLOCK       *pstSecondsClock;
	orxCLOCK 	   *pstScoreDisplayClock;
	
	orxU32          i;
	orxINPUT_TYPE   eType;
	orxENUM         eID;

	leftFlipperPressed = orxFALSE;
	rightFlipperPressed = orxFALSE;
	launcherTrapOn = orxFALSE;
	//leftTrapOn = orxFALSE;
	//rightTrapOn = orxFALSE;
	ballInPlay = orxFALSE;

	/* Multiball variables that need to be filled to award multiball. */
	launchMultiBalls = orxFALSE;
	rollOversFilled = orxFALSE;
	targetGroupRightFilled = orxFALSE;

	score = 0;
	balls = 0;

	/* Creates viewport */
	pstViewport = orxViewport_CreateFromConfig("Viewport");


	/* Gets camera */
	pstCamera = orxViewport_GetCamera(pstViewport);
	//orxCamera_SetZoom(pstCamera, nativeScreenHeight/1280);

	SetFrustumToWhateverDisplaySizeCurrentlyIs();

	/* Registers event handler */
	orxEvent_AddHandler(orxEVENT_TYPE_ANIM, AnimationEventHandlerDispatcher);
	orxEvent_AddHandler(orxEVENT_TYPE_PHYSICS, PhysicsEventHandlerDispatcher);
	orxEvent_AddHandler(orxEVENT_TYPE_INPUT, InputEventHandlerDispatcher);
	orxEvent_AddHandler(orxEVENT_TYPE_SPAWNER, EventHandlerDispatcher);
	orxEvent_AddHandler(orxEVENT_TYPE_SYSTEM, EventHandlerDispatcher);
	orxEvent_AddHandler(orxEVENT_TYPE_VIEWPORT, EventHandlerDispatcher);
	orxEvent_AddHandler(orxEVENT_TYPE_OBJECT, EventHandlerDispatcher);
	orxEvent_AddHandler(orxEVENT_TYPE_USER_DEFINED, CustomEventHandlerDispatcher);

	orxEvent_AddHandler(orxEVENT_TYPE_RENDER, EventHandlerDispatcher);




	tableObject = orxObject_CreateFromConfig("TableObject");
	tableSoundCentrePosition = orxObject_CreateFromConfig("TableSoundCentrePosition");

	SetProgressPercent(0);

	leftFlipperObject = orxObject_CreateFromConfig("LeftFlipperObject");
	rightFlipperObject = orxObject_CreateFromConfig("RightFlipperObject");
	
	orxObject_SetParent(leftFlipperObject, tableObject);
	orxObject_SetParent(rightFlipperObject, tableObject);

	
	ballCatcherObject = orxObject_CreateFromConfig("BallCatcherObject");

	trapObject = orxObject_CreateFromConfig("TrapObject");
	SetLauncherTrap(orxFALSE);

	//trapLeftObject = orxObject_CreateFromConfig("TrapLeftObject");
	//trapRightObject = orxObject_CreateFromConfig("TrapRightObject");

	TurnOffAllMultiplierLights();

	directionrolloverelement *launcherRollOver = new directionrolloverelement(50, (orxCHAR*)"BALL_LAUNCHED", orxNULL);
	//directionrolloverelement *leftChannelRollOver = new directionrolloverelement(50, orxTRUE, (orxCHAR*)"LEFT_FLUKE_SHOT", orxNULL);
	//leftChannelRollOver->SetActivatorPosition(142, 982);
	//directionrolloverelement *rightChannelRollOver = new directionrolloverelement(50, orxTRUE, (orxCHAR*)"RIGHT_FLUKE_SHOT", orxNULL);
	//rightChannelRollOver->SetActivatorPosition(797, 982);

	
	int tgX = 222;
	int tgY = 416;


	
	scoreObject = orxObject_CreateFromConfig("LedObject");
	orxObject_SetParent(scoreObject, tableObject);
	
	ballCountObject = orxObject_CreateFromConfig("BallLedCounterObject");
	orxObject_SetParent(ballCountObject, tableObject);


	leftTouchObject = orxObject_CreateFromConfig("TouchHighlightLeftObject");
	rightTouchObject = orxObject_CreateFromConfig("TouchHighlightRightObject");
	swipeTouchObject = orxObject_CreateFromConfig("TouchHighlightSwipeObject");

	SetTouchHighlightPositions(deviceWidth, deviceHeight);

	orxObject_Enable(leftTouchObject, orxTRUE);
	orxObject_Enable(rightTouchObject, orxTRUE);

	plung = new plunger();
	plung->ParentTo(tableObject);
	
	orxMath_InitRandom((orxS32)orxSystem_GetRealTime());

	int lastScore = GetSavedHighScore();

	score = lastScore;

	orxVector_Set(&lastAccel, orxInput_GetValue("AccelX"), orxInput_GetValue("AccelY"), orxInput_GetValue("AccelZ"));	

	ShowTitle();
	//orxObject_CreateFromConfig("TestObject");

	/* Done! */

	/* clocks */
	pstClock = orxClock_Create(orx2F(0.01f), orxCLOCK_TYPE_USER);
	pstSecondsClock = orxClock_Create(orx2F(1.0f), orxCLOCK_TYPE_USER);
	pstDancingLightsClock = orxClock_Create(orx2F(0.35f), orxCLOCK_TYPE_USER);
	pstScoreDisplayClock = orxClock_Create(orx2F(0.05f), orxCLOCK_TYPE_USER);

	/* Registers callbacks */
	orxClock_Register(pstClock, UpdateDispatcher, orxNULL, orxMODULE_ID_MAIN, orxCLOCK_PRIORITY_NORMAL);
	orxClock_Register(pstSecondsClock, SecondsUpdateDispatcher, orxNULL, orxMODULE_ID_MAIN, orxCLOCK_PRIORITY_NORMAL);
	orxClock_Register(pstDancingLightsClock, DancingLightsUpdateDispatcher, orxNULL, orxMODULE_ID_MAIN, orxCLOCK_PRIORITY_NORMAL);
	//orxClock_Pause(pstDancingLightsClock);

	orxClock_Register(pstScoreDisplayClock, ScoreUpdaterDispatcher, orxNULL, orxMODULE_ID_MAIN, orxCLOCK_PRIORITY_NORMAL);

}
