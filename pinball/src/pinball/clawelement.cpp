#include "orx.h"
#include "element.h"
#include "clawelement.h"

clawelement::clawelement(	int points, orxSTRING activatorNameFromConfig, orxSTRING activatorAnimationNameFromConfig, 
							orxSTRING activatorHitAnimationNameFromConfig, orxSTRING lightNameFromConfig)
							: element(points, activatorNameFromConfig, lightNameFromConfig) {
	activatorAnimationName = activatorAnimationNameFromConfig;
	activatorHitAnimationName = activatorHitAnimationNameFromConfig;
	lightOnAnimationName = lightNameFromConfig;
	lightFXName = (orxCHAR*)"ShieldArrowFX";
	activatorFlashAnimationName = orxNULL;
	
	clawIsActive = orxFALSE;
	ballIsCaptured = orxFALSE;
	orxObject_Enable (light, clawIsActive);
	
	capturedBall = orxNULL;

}

clawelement::~clawelement()
{
}

void clawelement::CompressClaw()
{
	orxObject_SetCurrentAnim(activator, activatorHitAnimationName);
}

void clawelement::ActivateClaw(){
	if (clawIsActive == orxTRUE || ballIsCaptured == orxTRUE){
		return; //don't set it if already set or ball is captured.
	}
	
	clawIsActive = orxTRUE;
	orxObject_Enable (light, clawIsActive);
	
	if (this->LightExists()){
		orxObject_AddFX(light, this->lightFXName);
	}
}

void clawelement::DeactivateClaw(){
	clawIsActive = orxFALSE;
	orxObject_Enable (light, clawIsActive);
	
	if (this->LightExists()){
		orxObject_RemoveFX(light, this->lightFXName);
	}
	
	if (BallIsCaptured()){
		DestroyCapturedBall();
	}
}

orxBOOL clawelement::ClawIsActive(){
	return clawIsActive;
}

orxBOOL clawelement::BallIsCaptured(){
	return ballIsCaptured;
}

void clawelement::SetCapturedBall(orxOBJECT *ball){
	capturedBall = ball;
	ballIsCaptured = orxTRUE;
	
	orxBODY *body = orxOBJECT_GET_STRUCTURE(capturedBall, BODY);
	orxBODY_PART *part = orxBody_GetNextPart(body, orxNULL);
	orxBody_SetPartSolid(part, orxFALSE);
	
	orxVECTOR zero = {0, 0, 0};
	
	orxObject_SetCustomGravity(capturedBall, &zero);
	orxObject_SetSpeed(capturedBall, &zero);
	
	orxVECTOR ballPosition = {0,0,0};
	orxObject_GetPosition(capturedBall, &ballPosition);
	
	if (orxString_Compare(        this->GetClawType(), "RIGHT"        )   == 0){
		ballPosition.fX = 715;//104.887077
		ballPosition.fY = 174;//614.01947
	} else if(orxString_Compare(        this->GetClawType(), "TOP"        )   == 0){
		ballPosition.fX = 722;//104.887077
		ballPosition.fY = 44;//614.01947
	} else {
		ballPosition.fX = 109;//104.887077
		ballPosition.fY = 585;//614.01947	
	}
	

	
	orxObject_SetPosition(capturedBall, &ballPosition);
	
}

void clawelement::DropCapturedBall(){
	ballIsCaptured = orxFALSE;
	
	if (capturedBall != orxNULL){
		orxBODY *body = orxOBJECT_GET_STRUCTURE(capturedBall, BODY);
		orxBODY_PART *part = orxBody_GetNextPart(body, orxNULL);
		orxBody_SetPartSolid(part, orxTRUE);
		
		orxObject_SetCustomGravity(capturedBall, orxNULL);
		
		if (orxString_Compare(    this->GetClawType(), "TOP"    )   == 0){
			orxVECTOR kickOutVector = {-500, 0, 0};
			orxObject_SetSpeed(capturedBall, &kickOutVector);
		}
	}
	
	capturedBall = orxNULL;
}

void clawelement::DestroyCapturedBall(){
	ballIsCaptured = orxFALSE;
	
	if (capturedBall != orxNULL){
		orxObject_SetLifeTime(capturedBall, 0);
	}
	
	capturedBall = orxNULL;
}


orxOBJECT* clawelement::GetCapturedBall(){
	return capturedBall;
}

orxSTRING clawelement::GetClawType(){
	
	if ( orxString_Compare(        lightOnAnimationName, "LeftClawArrowObject"        )   == 0 ){
		return "LEFT";
	} else if (orxString_Compare(        lightOnAnimationName, "RightClawArrowObject"        )   == 0 ){
		return "RIGHT";
	} 
	
	return "TOP";
}
