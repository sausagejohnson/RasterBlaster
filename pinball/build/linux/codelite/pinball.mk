##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Release_x32
ProjectName            :=pinball
ConfigurationName      :=Release_x32
IntermediateDirectory  :=$(ConfigurationName)
OutDir                 := $(IntermediateDirectory)
WorkspacePath          := "/work/Development/orx/pin/pinball/build/linux/codelite"
ProjectPath            := "/work/Development/orx/pin/pinball/build/linux/codelite"
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=root
Date                   :=09/18/2014
CodeLitePath           :="/root/.codelite"
LinkerName             :=g++
ArchiveTool            :=ar rcus
SharedObjectLinkerName :=g++ -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.o.i
DebugSwitch            :=-gstab
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
CompilerName           :=g++
C_CompilerName         :=gcc
OutputFile             :=../../../bin/linux32/pinball
Preprocessors          :=
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E 
ObjectsFileList        :="/work/Development/orx/pin/pinball/build/linux/codelite/pinball.txt"
PCHCompileFlags        :=
MakeDirCommand         :=mkdir -p
CmpOptions             := -msse2 -ffast-math -g -O2 -m32 -fno-exceptions -fno-rtti -fschedule-insns $(Preprocessors)
C_CmpOptions           := -msse2 -ffast-math -g -O2 -m32 -fno-exceptions -fno-rtti -fschedule-insns $(Preprocessors)
LinkOptions            :=  -m32 -L/usr/lib32 -Wl,-rpath ./ -Wl,--export-dynamic
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch)../../../include 
IncludePCH             := 
RcIncludePath          := 
Libs                   := $(LibrarySwitch)orx $(LibrarySwitch)dl $(LibrarySwitch)m $(LibrarySwitch)rt 
LibPath                := $(LibraryPathSwitch). $(LibraryPathSwitch)../../../lib/linux32 $(LibraryPathSwitch). 


##
## User defined environment variables
##
CodeLiteDir:=/usr/share/codelite
Objects=$(IntermediateDirectory)/pinball_bumperelement$(ObjectSuffix) $(IntermediateDirectory)/pinball_directionrolloverelement$(ObjectSuffix) $(IntermediateDirectory)/pinball_element$(ObjectSuffix) $(IntermediateDirectory)/pinball_elementgroup$(ObjectSuffix) $(IntermediateDirectory)/pinball_pinball$(ObjectSuffix) $(IntermediateDirectory)/pinball_plunger$(ObjectSuffix) $(IntermediateDirectory)/pinball_rolloverlightelement$(ObjectSuffix) $(IntermediateDirectory)/pinball_slingshotelement$(ObjectSuffix) $(IntermediateDirectory)/pinball_targetleftelement$(ObjectSuffix) $(IntermediateDirectory)/pinball_targetrightelement$(ObjectSuffix) \
	

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild
all: $(OutputFile)

$(OutputFile): $(IntermediateDirectory)/.d $(Objects) 
	@$(MakeDirCommand) $(@D)
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects) > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

$(IntermediateDirectory)/.d:
	@test -d $(ConfigurationName) || $(MakeDirCommand) $(ConfigurationName)

PreBuild:


##
## Objects
##
$(IntermediateDirectory)/pinball_bumperelement$(ObjectSuffix): ../../../src/pinball/bumperelement.cpp $(IntermediateDirectory)/pinball_bumperelement$(DependSuffix)
	$(CompilerName) $(IncludePCH) $(SourceSwitch) "/work/Development/orx/pin/pinball/src/pinball/bumperelement.cpp" $(CmpOptions) $(ObjectSwitch)$(IntermediateDirectory)/pinball_bumperelement$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/pinball_bumperelement$(DependSuffix): ../../../src/pinball/bumperelement.cpp
	@$(CompilerName) $(CmpOptions) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/pinball_bumperelement$(ObjectSuffix) -MF$(IntermediateDirectory)/pinball_bumperelement$(DependSuffix) -MM "/work/Development/orx/pin/pinball/src/pinball/bumperelement.cpp"

$(IntermediateDirectory)/pinball_bumperelement$(PreprocessSuffix): ../../../src/pinball/bumperelement.cpp
	@$(CompilerName) $(CmpOptions) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/pinball_bumperelement$(PreprocessSuffix) "/work/Development/orx/pin/pinball/src/pinball/bumperelement.cpp"

$(IntermediateDirectory)/pinball_directionrolloverelement$(ObjectSuffix): ../../../src/pinball/directionrolloverelement.cpp $(IntermediateDirectory)/pinball_directionrolloverelement$(DependSuffix)
	$(CompilerName) $(IncludePCH) $(SourceSwitch) "/work/Development/orx/pin/pinball/src/pinball/directionrolloverelement.cpp" $(CmpOptions) $(ObjectSwitch)$(IntermediateDirectory)/pinball_directionrolloverelement$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/pinball_directionrolloverelement$(DependSuffix): ../../../src/pinball/directionrolloverelement.cpp
	@$(CompilerName) $(CmpOptions) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/pinball_directionrolloverelement$(ObjectSuffix) -MF$(IntermediateDirectory)/pinball_directionrolloverelement$(DependSuffix) -MM "/work/Development/orx/pin/pinball/src/pinball/directionrolloverelement.cpp"

$(IntermediateDirectory)/pinball_directionrolloverelement$(PreprocessSuffix): ../../../src/pinball/directionrolloverelement.cpp
	@$(CompilerName) $(CmpOptions) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/pinball_directionrolloverelement$(PreprocessSuffix) "/work/Development/orx/pin/pinball/src/pinball/directionrolloverelement.cpp"

$(IntermediateDirectory)/pinball_element$(ObjectSuffix): ../../../src/pinball/element.cpp $(IntermediateDirectory)/pinball_element$(DependSuffix)
	$(CompilerName) $(IncludePCH) $(SourceSwitch) "/work/Development/orx/pin/pinball/src/pinball/element.cpp" $(CmpOptions) $(ObjectSwitch)$(IntermediateDirectory)/pinball_element$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/pinball_element$(DependSuffix): ../../../src/pinball/element.cpp
	@$(CompilerName) $(CmpOptions) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/pinball_element$(ObjectSuffix) -MF$(IntermediateDirectory)/pinball_element$(DependSuffix) -MM "/work/Development/orx/pin/pinball/src/pinball/element.cpp"

$(IntermediateDirectory)/pinball_element$(PreprocessSuffix): ../../../src/pinball/element.cpp
	@$(CompilerName) $(CmpOptions) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/pinball_element$(PreprocessSuffix) "/work/Development/orx/pin/pinball/src/pinball/element.cpp"

$(IntermediateDirectory)/pinball_elementgroup$(ObjectSuffix): ../../../src/pinball/elementgroup.cpp $(IntermediateDirectory)/pinball_elementgroup$(DependSuffix)
	$(CompilerName) $(IncludePCH) $(SourceSwitch) "/work/Development/orx/pin/pinball/src/pinball/elementgroup.cpp" $(CmpOptions) $(ObjectSwitch)$(IntermediateDirectory)/pinball_elementgroup$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/pinball_elementgroup$(DependSuffix): ../../../src/pinball/elementgroup.cpp
	@$(CompilerName) $(CmpOptions) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/pinball_elementgroup$(ObjectSuffix) -MF$(IntermediateDirectory)/pinball_elementgroup$(DependSuffix) -MM "/work/Development/orx/pin/pinball/src/pinball/elementgroup.cpp"

$(IntermediateDirectory)/pinball_elementgroup$(PreprocessSuffix): ../../../src/pinball/elementgroup.cpp
	@$(CompilerName) $(CmpOptions) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/pinball_elementgroup$(PreprocessSuffix) "/work/Development/orx/pin/pinball/src/pinball/elementgroup.cpp"

$(IntermediateDirectory)/pinball_pinball$(ObjectSuffix): ../../../src/pinball/pinball.cpp $(IntermediateDirectory)/pinball_pinball$(DependSuffix)
	$(CompilerName) $(IncludePCH) $(SourceSwitch) "/work/Development/orx/pin/pinball/src/pinball/pinball.cpp" $(CmpOptions) $(ObjectSwitch)$(IntermediateDirectory)/pinball_pinball$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/pinball_pinball$(DependSuffix): ../../../src/pinball/pinball.cpp
	@$(CompilerName) $(CmpOptions) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/pinball_pinball$(ObjectSuffix) -MF$(IntermediateDirectory)/pinball_pinball$(DependSuffix) -MM "/work/Development/orx/pin/pinball/src/pinball/pinball.cpp"

$(IntermediateDirectory)/pinball_pinball$(PreprocessSuffix): ../../../src/pinball/pinball.cpp
	@$(CompilerName) $(CmpOptions) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/pinball_pinball$(PreprocessSuffix) "/work/Development/orx/pin/pinball/src/pinball/pinball.cpp"

$(IntermediateDirectory)/pinball_plunger$(ObjectSuffix): ../../../src/pinball/plunger.cpp $(IntermediateDirectory)/pinball_plunger$(DependSuffix)
	$(CompilerName) $(IncludePCH) $(SourceSwitch) "/work/Development/orx/pin/pinball/src/pinball/plunger.cpp" $(CmpOptions) $(ObjectSwitch)$(IntermediateDirectory)/pinball_plunger$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/pinball_plunger$(DependSuffix): ../../../src/pinball/plunger.cpp
	@$(CompilerName) $(CmpOptions) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/pinball_plunger$(ObjectSuffix) -MF$(IntermediateDirectory)/pinball_plunger$(DependSuffix) -MM "/work/Development/orx/pin/pinball/src/pinball/plunger.cpp"

$(IntermediateDirectory)/pinball_plunger$(PreprocessSuffix): ../../../src/pinball/plunger.cpp
	@$(CompilerName) $(CmpOptions) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/pinball_plunger$(PreprocessSuffix) "/work/Development/orx/pin/pinball/src/pinball/plunger.cpp"

$(IntermediateDirectory)/pinball_rolloverlightelement$(ObjectSuffix): ../../../src/pinball/rolloverlightelement.cpp $(IntermediateDirectory)/pinball_rolloverlightelement$(DependSuffix)
	$(CompilerName) $(IncludePCH) $(SourceSwitch) "/work/Development/orx/pin/pinball/src/pinball/rolloverlightelement.cpp" $(CmpOptions) $(ObjectSwitch)$(IntermediateDirectory)/pinball_rolloverlightelement$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/pinball_rolloverlightelement$(DependSuffix): ../../../src/pinball/rolloverlightelement.cpp
	@$(CompilerName) $(CmpOptions) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/pinball_rolloverlightelement$(ObjectSuffix) -MF$(IntermediateDirectory)/pinball_rolloverlightelement$(DependSuffix) -MM "/work/Development/orx/pin/pinball/src/pinball/rolloverlightelement.cpp"

$(IntermediateDirectory)/pinball_rolloverlightelement$(PreprocessSuffix): ../../../src/pinball/rolloverlightelement.cpp
	@$(CompilerName) $(CmpOptions) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/pinball_rolloverlightelement$(PreprocessSuffix) "/work/Development/orx/pin/pinball/src/pinball/rolloverlightelement.cpp"

$(IntermediateDirectory)/pinball_slingshotelement$(ObjectSuffix): ../../../src/pinball/slingshotelement.cpp $(IntermediateDirectory)/pinball_slingshotelement$(DependSuffix)
	$(CompilerName) $(IncludePCH) $(SourceSwitch) "/work/Development/orx/pin/pinball/src/pinball/slingshotelement.cpp" $(CmpOptions) $(ObjectSwitch)$(IntermediateDirectory)/pinball_slingshotelement$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/pinball_slingshotelement$(DependSuffix): ../../../src/pinball/slingshotelement.cpp
	@$(CompilerName) $(CmpOptions) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/pinball_slingshotelement$(ObjectSuffix) -MF$(IntermediateDirectory)/pinball_slingshotelement$(DependSuffix) -MM "/work/Development/orx/pin/pinball/src/pinball/slingshotelement.cpp"

$(IntermediateDirectory)/pinball_slingshotelement$(PreprocessSuffix): ../../../src/pinball/slingshotelement.cpp
	@$(CompilerName) $(CmpOptions) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/pinball_slingshotelement$(PreprocessSuffix) "/work/Development/orx/pin/pinball/src/pinball/slingshotelement.cpp"

$(IntermediateDirectory)/pinball_targetleftelement$(ObjectSuffix): ../../../src/pinball/targetleftelement.cpp $(IntermediateDirectory)/pinball_targetleftelement$(DependSuffix)
	$(CompilerName) $(IncludePCH) $(SourceSwitch) "/work/Development/orx/pin/pinball/src/pinball/targetleftelement.cpp" $(CmpOptions) $(ObjectSwitch)$(IntermediateDirectory)/pinball_targetleftelement$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/pinball_targetleftelement$(DependSuffix): ../../../src/pinball/targetleftelement.cpp
	@$(CompilerName) $(CmpOptions) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/pinball_targetleftelement$(ObjectSuffix) -MF$(IntermediateDirectory)/pinball_targetleftelement$(DependSuffix) -MM "/work/Development/orx/pin/pinball/src/pinball/targetleftelement.cpp"

$(IntermediateDirectory)/pinball_targetleftelement$(PreprocessSuffix): ../../../src/pinball/targetleftelement.cpp
	@$(CompilerName) $(CmpOptions) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/pinball_targetleftelement$(PreprocessSuffix) "/work/Development/orx/pin/pinball/src/pinball/targetleftelement.cpp"

$(IntermediateDirectory)/pinball_targetrightelement$(ObjectSuffix): ../../../src/pinball/targetrightelement.cpp $(IntermediateDirectory)/pinball_targetrightelement$(DependSuffix)
	$(CompilerName) $(IncludePCH) $(SourceSwitch) "/work/Development/orx/pin/pinball/src/pinball/targetrightelement.cpp" $(CmpOptions) $(ObjectSwitch)$(IntermediateDirectory)/pinball_targetrightelement$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/pinball_targetrightelement$(DependSuffix): ../../../src/pinball/targetrightelement.cpp
	@$(CompilerName) $(CmpOptions) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/pinball_targetrightelement$(ObjectSuffix) -MF$(IntermediateDirectory)/pinball_targetrightelement$(DependSuffix) -MM "/work/Development/orx/pin/pinball/src/pinball/targetrightelement.cpp"

$(IntermediateDirectory)/pinball_targetrightelement$(PreprocessSuffix): ../../../src/pinball/targetrightelement.cpp
	@$(CompilerName) $(CmpOptions) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/pinball_targetrightelement$(PreprocessSuffix) "/work/Development/orx/pin/pinball/src/pinball/targetrightelement.cpp"


-include $(IntermediateDirectory)/*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) $(IntermediateDirectory)/pinball_bumperelement$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/pinball_bumperelement$(DependSuffix)
	$(RM) $(IntermediateDirectory)/pinball_bumperelement$(PreprocessSuffix)
	$(RM) $(IntermediateDirectory)/pinball_directionrolloverelement$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/pinball_directionrolloverelement$(DependSuffix)
	$(RM) $(IntermediateDirectory)/pinball_directionrolloverelement$(PreprocessSuffix)
	$(RM) $(IntermediateDirectory)/pinball_element$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/pinball_element$(DependSuffix)
	$(RM) $(IntermediateDirectory)/pinball_element$(PreprocessSuffix)
	$(RM) $(IntermediateDirectory)/pinball_elementgroup$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/pinball_elementgroup$(DependSuffix)
	$(RM) $(IntermediateDirectory)/pinball_elementgroup$(PreprocessSuffix)
	$(RM) $(IntermediateDirectory)/pinball_pinball$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/pinball_pinball$(DependSuffix)
	$(RM) $(IntermediateDirectory)/pinball_pinball$(PreprocessSuffix)
	$(RM) $(IntermediateDirectory)/pinball_plunger$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/pinball_plunger$(DependSuffix)
	$(RM) $(IntermediateDirectory)/pinball_plunger$(PreprocessSuffix)
	$(RM) $(IntermediateDirectory)/pinball_rolloverlightelement$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/pinball_rolloverlightelement$(DependSuffix)
	$(RM) $(IntermediateDirectory)/pinball_rolloverlightelement$(PreprocessSuffix)
	$(RM) $(IntermediateDirectory)/pinball_slingshotelement$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/pinball_slingshotelement$(DependSuffix)
	$(RM) $(IntermediateDirectory)/pinball_slingshotelement$(PreprocessSuffix)
	$(RM) $(IntermediateDirectory)/pinball_targetleftelement$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/pinball_targetleftelement$(DependSuffix)
	$(RM) $(IntermediateDirectory)/pinball_targetleftelement$(PreprocessSuffix)
	$(RM) $(IntermediateDirectory)/pinball_targetrightelement$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/pinball_targetrightelement$(DependSuffix)
	$(RM) $(IntermediateDirectory)/pinball_targetrightelement$(PreprocessSuffix)
	$(RM) $(OutputFile)
	$(RM) "/work/Development/orx/pin/pinball/build/linux/codelite/.build-release_x32/pinball"


