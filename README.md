# README #

Thanks for dropping by the development repository for Raster Blaster, part of the Hip Pocket Pinball series.

### What is this repository for? ###

* Anyone wanting to download a version for their desktop or mobile.
* Source is available if you want to tinker around with Orx.
* Versions available for Windows, Linux, Mac OSX and Android. IOS in not planned.

### How do I get set up? ###

* For Android, download the .apk file and install it(when it becomes available). It doesn't require any permissions as yet. You will need to enable "Unknown Sources" to play it, but your device will prompt you when you try to install it.
* For Desktop, there is no installer yet. Just unpack the archive and make your way into pinball/bin/<MY_OS>/pinball.exe

### Controls ###
* For desktop, the keys to play are shown on the title screens. You can pull back the plunger, or wack the plunger forward to launch the ball. 
* For mobile, swipe down to vary the plunger and let go to launch. Flipper touchzones are in the bottom left and right of the screen.

### Contribution guidelines ###

* I would love your feedback on the game: sausage@zeta.org.au. And please tell your friends. 
* You're welcome to dig through code to help learn about the Orx Portable Game Engine.

### Who do I talk to? ###

* Wayne sausage@zeta.org.au
* Or catch me on the forums at: http://www.orx-project.org/forum